#include <time.h>
#include "get_ready.h"

//global vars needed by diffrent functions
extern SDL_Texture* wall1;
extern SDL_Texture* wall2;
extern SDL_Texture* displayscore;
extern SDL_Rect birdpos;
extern SDL_Rect pipe1;
extern SDL_Rect pipe2;
extern TTF_Font* font;

//function prototypes
void playgame();
void loadpipes();
void helibird(SDL_Event e);
void bringpipes();
void generatepipes();
void checkhit();
void resetbirdpos();
void showscore(bool moveposition);
