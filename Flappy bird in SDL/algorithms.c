#include <stdio.h>
#include <stdlib.h>
#include "algorithms.h"

/*---------------------------------------------------------
                    Bubble Sort Algorithm
Loops through array for every single element and if the next
element is less than the current, then swaps them until sorted
-----------------------------------------------------------*/
void bubbleSort(int *array, int length){
    int i, k=1, temp=0;
    while (k!=0){
        k=0;
        for (i = 0; i<length-1; i++) {
            if (array[i]>array[i+1]){
                temp=array[i];
                array[i]=array[i+1];
                array[i+1]=temp;
                k=1;
            }
        }
    }
}

/*---------------------------------------------------------
                Selection Sort Algorithm
Finds the minimum and puts it at the beginning of array by
doing a full loop for every single element in the array
-----------------------------------------------------------*/
void selectionsort(int *array, int length){
    int i, k, min, temp;
    for (i=0; i<length-1; i++){
        min=i;
        //finds the minimum
        for(k=i+1; k<length; k++){
            if (array[k]<array[min]){
                min=k;
            }
        }
        //swaps the current and minimum elements
        temp=array[i];
        array[i]=array[min];
        array[min]=temp;
    }
}

/*---------------------------------------------------------
         Insertion Sort Algorithm (efficient V)
inserts elements into the left side of the array, creates
holes for each element and if previous elements are >
than the current value, it shifts all elements to the right
and then puts the value back into its relative position
-----------------------------------------------------------*/
void insertionsort(int *array, int length){
    int i, hole, value;
    for (i=0; i<length; i++){
        hole=i;
        value=array[i];
        //shift elements to the right
        while(hole>0 && array[hole-1]>value){
            array[hole]=array[hole-1];
            hole--;
        }
        //places hole value where it belongs
        array[hole]=value;
    }
}

/*---------------------------------------------------------
                    Mergesort Algorithm
uses recursion to keep splitting array into 2 and when every
single element has been divided, it merges back each element
or subset array back into its parent array in a sorted manner
-----------------------------------------------------------*/
void mergesort(int *array, int length){
    if (length<2) return;
    int lsize=length/2;
    int rsize=length-lsize;
    //create two subset arrays and allocate memory
    int i, *larray, *rarray;
    larray=(int*)malloc(lsize*sizeof(int));
    rarray=(int*)malloc(rsize*sizeof(int));
    //insert elements from parent array into subset arryas
    for (i=0; i<lsize; i++){
        larray[i]=array[i];
    }
    for (i=lsize; i<length; i++){
        rarray[i-lsize]=array[i];
    }

    //recursive call on each side
    mergesort(larray, lsize);
    mergesort(rarray, rsize);
    //when down to 1 element, then merge
    merge(larray, rarray, array, lsize, rsize);
    free(larray);
    free(rarray);
}

/*---------------------------------------------------------
            merge function for Mergesort
takes elements from subset arrays and puts them back into
parent array in sorted order
-----------------------------------------------------------*/
void merge(int *larray, int *rarray, int *array, int lsize, int rsize){
    int i=0, j=0, k=0;
    //ovveride elements in parent array
    while (i<lsize && j<rsize){
        if (larray[i]<=rarray[j]){
            array[k++]=larray[i++];
        } else{
            array[k++]=rarray[j++];
        }
    }

    //if odd # of elements, then put those aswell
    while (i<lsize){
        array[k++]=larray[i++];
    }
    while (j<lsize){
        array[k++]=rarray[j++];
    }
}

/*---------------------------------------------------------
                    Quick Sort Algorithm
selects a random element in the array and moves it to a certain
position in the array such that all elements less than the random
element are to the left and elements greater are to the right.
more efficient as it uses recursion and doesnt use extra memory
-----------------------------------------------------------*/
void quicksort(int *array, int start, int end){
    //exit condition for recursion
    if (start>=end) return;
    //splits the array and returns the index at which the
    //random element has been placed
    int index=split(array,start,end);

    //recursion on left and right side of array
    quicksort(array,start, index-1);
    quicksort(array,index+1,end);
}

/*---------------------------------------------------------
            splitting function for Quick Sort
rearranges elements so that everything to the left is less than
the random element while everything on the right is greater
-----------------------------------------------------------*/
int split(int *array, int start, int end){
    //selects last element for comparison
    int random=array[end], index=start;
    int i, temp;
    //compares and swaps elements
    for (i=start; i<end; i++){
        if (array[i]<random){
            temp=array[index];
            array[index]=array[i];
            array[i]=temp;
            index++;
        }
    }

    temp=array[index];
    array[index]=random;
    array[end]=temp;
    return index;
}
