#include "home_page.h"

//global variables
SDL_Texture* playbtn=NULL;
SDL_Texture* title=NULL;
SDL_Texture* ground=NULL;
SDL_Texture* background=NULL;
SDL_Texture* birdtexture=NULL;
SDL_Texture* sortbtn=NULL;
int frame=0, reverse=0, pic=0;
bool shutdown=false;
bool quit, exitgame,sort;
SDL_Event e;

/*---------------------------------------------------------
 renders all content for home page along with animation and
 interactivity. the user has to select to either play the
 game or go to sorting algorithms in which case the window
 will close.
---------------------------------------------------------*/
bool render_homepage(){
    quit=false;
    exitgame=false;
    sort=false;
    loadmedia();
    //loop for home_page
    while(quit!=true){
        while(SDL_PollEvent(&e)!=0){
            if(e.type==SDL_QUIT){
                quit=true;
                exitgame=true;
                shutdown=true;
            }
        }
        SDL_RenderClear(render);
        showbackground(true);
        showground(true);
        showbird(true,true);
        showtitle();
        mousepressed();
        sorting_algorithm();
        SDL_RenderPresent(render);
        frame++;
    }
    frame=0;
    fadehome(exitgame);
    return sort;
}

/*---------------------------------------------------------
handles mouse events for the play button. the button will
change colour if the mouse is hovered over it and if the
button is pressed, the button shrinks in size simulating
an animation. when clicked, operation passes to next page
---------------------------------------------------------*/
SDL_Rect play_screen={20,340,114,65};
int x=0, y=0;
void mousepressed(){
    SDL_GetMouseState(&x,&y);
    temp=IMG_Load("graphics/btn1.png");
    if(x>20 && x<134 && y>340 && y<405){
        if(e.type==SDL_MOUSEMOTION){
            temp=IMG_Load("graphics/btn2.png");
        }else if(e.type==SDL_MOUSEBUTTONDOWN){
            temp=IMG_Load("graphics/btnpressed.png");
            if(play_screen.x!=30 && play_screen.w!=104 && play_screen.h!=55){
                play_screen.x+=10;
                play_screen.w-=10;
                play_screen.h-=10;
            }
        }else if(e.type==SDL_MOUSEBUTTONUP){
            temp=IMG_Load("graphics/btn3.png");
            if(play_screen.x!=20 && play_screen.w!=114 && play_screen.h!=65){
                play_screen.x-=10;
                play_screen.w+=10;
                play_screen.h+=10;
            }
            quit=true;
        }
    }
    playbtn=SDL_CreateTextureFromSurface(render,temp);
    SDL_RenderCopy(render,playbtn,NULL,&play_screen);
}

/*---------------------------------------------------------
handles mouse events for the sorting button and simulates
similar animation as the play button and, when clicked,
quits sdl and starts sorting_algorithm in terminal.
---------------------------------------------------------*/
SDL_Rect sort_btn={154,340,114,65};
void sorting_algorithm(){
    SDL_GetMouseState(&x,&y);
    temp=IMG_Load("graphics/sort.png");
    if(x>154 && x<268 && y>340 && y<405){
        if(e.type==SDL_MOUSEMOTION){
            temp=IMG_Load("graphics/sort_over.png");
        }else if(e.type==SDL_MOUSEBUTTONDOWN){
            quit=true;
            sort=true;
        }
    }
    sortbtn=SDL_CreateTextureFromSurface(render,temp);
    SDL_RenderCopy(render,sortbtn,NULL,&sort_btn);
}

/*---------------------------------------------------------
renders background and animates it by slowly clipping the
texture background i.e. the x axis is incremented and when
it reaches the end of the background, its set back to 0
resulting in a smooth animation.
---------------------------------------------------------*/
SDL_Rect background_screen={0,0,288,512};
void showbackground(bool move){
    if (move==true){
        if(frame%15==0){
            if(background_screen.x<288){
                background_screen.x+=2;
            }else{
                background_screen.x=0;
            }
        }
    }
    SDL_RenderCopy(render,background,&background_screen,NULL);
}

/*---------------------------------------------------------
loads all graphics for this page into textures which are then
manipulated using SDL Rectangles.
---------------------------------------------------------*/
void loadmedia(){
    SDL_Surface* temp=IMG_Load("graphics/double-back.png");
    background=SDL_CreateTextureFromSurface(render,temp);

    temp=IMG_Load("graphics/ground.png");
    ground=SDL_CreateTextureFromSurface(render,temp);

    temp=IMG_Load("graphics/title.png");
    title=SDL_CreateTextureFromSurface(render,temp);

    temp=IMG_Load("graphics/play-btn.png");
    playbtn=SDL_CreateTextureFromSurface(render,temp);

    temp=IMG_Load("graphics/sort.png");
    sortbtn=SDL_CreateTextureFromSurface(render,temp);
}

/*---------------------------------------------------------
renders the ground animation in the same way as the background
animation is done i.e. the texture is clipped at diffrent
angles. to slow down the animation, the ground is only moved
after every 15 frames. the same logic is used to slow down
movement throughout the game.
---------------------------------------------------------*/
SDL_Rect bottom_screen={0,416,288,96};
SDL_Rect moveground={0,0,286,96};
void showground(bool move){
    if(move==true){
        if(frame%15==0){
            if(moveground.x<288){
                moveground.x+=2;
            }else{
                moveground.x=0;
            }
        }
    }
    SDL_RenderCopy(render,ground,&moveground,&bottom_screen);
}

/*---------------------------------------------------------
renders the bird animation by simply loading diffrent images
of the bird at diffrent frame intervals.
---------------------------------------------------------*/
SDL_Rect bird_screen={120,120,48,28};
char *bird1="birds/bird1.png";
char *bird2="birds/bird2.png";
char *bird3="birds/bird3.png";
void showbird(bool move,bool rendcopy){
    SDL_Surface* temp=IMG_Load(bird1);
    if(frame%(move ? 10 : 50)==0){
        if(pic==4)  pic=0;
        if (pic==0){
            temp=IMG_Load(bird1);
            birdtexture=SDL_CreateTextureFromSurface(render,temp);
        }else if(pic==1){
            temp=IMG_Load(bird2);
            birdtexture=SDL_CreateTextureFromSurface(render,temp);
        }else if(pic==2){
            temp=IMG_Load(bird3);
            birdtexture=SDL_CreateTextureFromSurface(render,temp);
        }else if(pic==3){
            temp=IMG_Load(bird2);
            birdtexture=SDL_CreateTextureFromSurface(render,temp);
        }
        pic++;
    }
    if(move==true){
        movebird_updown();
    }
    if(rendcopy==true){
        SDL_RenderCopy(render,birdtexture,NULL,&bird_screen);
    }
}

/*---------------------------------------------------------
moves the bird up and down on the home page in full
synchroniastion with the movement of the title. the y
co ordinates are simply modified.
---------------------------------------------------------*/
void movebird_updown(){
    if(frame%5==0){
        if(bird_screen.y<150 && reverse==0){
            bird_screen.y+=1;
        }else if(bird_screen.y==150 || reverse==1){
            bird_screen.y-=1;
            reverse=1;
            if(bird_screen.y==120){
                reverse=0;
            }
        }
    }
}

/*---------------------------------------------------------
     moves the title animation up and down the screen
---------------------------------------------------------*/
SDL_Rect title_screen={44,60,200,40};
void showtitle(){
    if(frame%5==0){
        if(title_screen.y<90 && reverse==0){
            title_screen.y+=1;
        }else if(title_screen.y==90 || reverse==1){
            title_screen.y-=1;
            reverse=1;
            if(title_screen.y==60){
                reverse=0;
            }
        }
    }
    SDL_RenderCopy(render,title,NULL,&title_screen);
}

/*---------------------------------------------------------
moves title out of screen and fades buttons. if the user wants
to play the game, rather than suddenly showing the next set of
graphics. the current textures are faded away to simulate a
smooth transition.
---------------------------------------------------------*/
int out=250;
bool transition(){
    if(frame%4==0){
        if(out>0){
            out-=10;
            SDL_SetTextureAlphaMod(playbtn,out);
            SDL_SetTextureAlphaMod(sortbtn,out);
        }
        if(title_screen.y>-40){
            title_screen.y-=10;
        }
    }
    SDL_RenderCopy(render,title,NULL,&title_screen);
    SDL_RenderCopy(render,playbtn,NULL,&play_screen);
    SDL_RenderCopy(render,sortbtn,NULL,&sort_btn);
    if(out<=0 && title_screen.y<=-40){
        return true;
    }
    return false;
}

/*---------------------------------------------------------
another event loop that slowly fades content into new page
---------------------------------------------------------*/
void fadehome(bool stop){
    while(stop!=true){
        while(SDL_PollEvent(&e)!=0){
            if(e.type==SDL_QUIT){
                stop=true;
                shutdown=true;
            }
        }
        SDL_RenderClear(render);
        showbackground(true);
        showground(true);
        showbird(false,true);
        stop=transition();
        SDL_RenderPresent(render);
        frame++;
    }
    frame=0;
}
