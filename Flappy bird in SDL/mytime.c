#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mytime.h"

/*-----------------------------------------------
            Benchmark Bubble Sort
-------------------------------------------------*/
double time_bubbleSort(int *array, int length){
    clock_t start, end;
    double timetaken=0;
    start=clock();
    bubbleSort(array, length);
    end=clock();
    timetaken=(double)(end-start)/CLOCKS_PER_SEC;
    return timetaken;
}

/*-----------------------------------------------
            Benchmark Selection Sort
-------------------------------------------------*/
double time_selectionsort(int *array, int length){
    clock_t start, end;
    double timetaken=0;
    start=clock();
    selectionsort(array, length);
    end=clock();
    timetaken=(double)(end-start)/CLOCKS_PER_SEC;
    return timetaken;
}

/*-----------------------------------------------
            Benchmark Insertion Sort
-------------------------------------------------*/
double time_insertionsort(int *array, int length){
    clock_t start, end;
    double timetaken=0;
    start=clock();
    insertionsort(array, length);
    end=clock();
    timetaken=(double)(end-start)/CLOCKS_PER_SEC;
    return timetaken;
}

/*-----------------------------------------------
            Benchmark Quick Sort
-------------------------------------------------*/
double time_quicksort(int *array, int start, int end){
    clock_t starttime, endtime;
    double timetaken=0;
    starttime=clock();
    quicksort(array,start,end);
    endtime=clock();
    timetaken=(double)(endtime-starttime)/CLOCKS_PER_SEC;
    return timetaken;
}

/*-----------------------------------------------
            Benchmark Merge Sort
-------------------------------------------------*/
double time_mergesort(int *array, int length){
    clock_t start, end;
    double timetaken=0;
    start=clock();
    mergesort(array, length);
    end=clock();
    timetaken=(double)(end-start)/CLOCKS_PER_SEC;
    return timetaken;
}

/*-----------------------------------------------
    Benchmark All Algorithms with same array
-------------------------------------------------*/
void testall(int *array, int length){
    int* sort=(int *)malloc(length*sizeof(int));
    double duration=0;
    fill(array, sort, length);

    printf("Quick Sort....\n");
    duration=time_quicksort(sort,0, length-1);
    printf("Time Taken %f in seconds\n\n",duration);
    fill(array, sort, length);

    printf("Merge Sort....\n");
    duration=time_mergesort(sort,length);
    printf("Time Taken %f in seconds\n\n",duration);
    fill(array, sort, length);

    printf("Insertion Sort....\n");
    duration=time_insertionsort(sort,length);
    printf("Time Taken %f in seconds\n\n",duration);
    fill(array, sort, length);

    printf("Selection Sort....\n");
    duration=time_selectionsort(sort,length);
    printf("Time Taken %f in seconds\n\n",duration);
    fill(array, sort, length);

    printf("Bubble Sort....\n");
    duration=time_bubbleSort(sort,length);
    printf("Time Taken %f in seconds\n\n",duration);
    free(sort);
}

/*-----------------------------------------------
  fill the sorted array back to unsorted version
-------------------------------------------------*/
void fill(int *original, int *array, int length){
    int i;
    for (i=0; i<length; i++){
        array[i]=original[i];
    }
}
