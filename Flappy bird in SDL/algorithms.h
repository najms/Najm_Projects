//function prototypes
void quicksort(int *array, int start, int end);
int split(int *array, int start, int end);
void mergesort(int *array, int length);
void merge(int *larray, int *rarray, int *array, int lsize, int rsize);
void selectionsort(int *array, int length);
void bubbleSort(int *array, int length);
void insertionsort(int *array, int length);
