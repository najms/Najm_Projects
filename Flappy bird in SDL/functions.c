#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "functions.h"

/*-----------------------------------------------
            Main menu for terminal
-------------------------------------------------*/
void mainmenu(int *array, int size){
    int opt=showmenu(size);
    double duration=0;
    switch(opt){
        case 0: exit(0);
            break;
        case 1: duration=time_bubbleSort(array, size);
                printf("Time Taken: %f in seconds\n", duration);
                break;
        case 2: duration=time_selectionsort(array, size);
                printf("Time Taken: %f in seconds\n", duration);
                break;
        case 3: duration=time_insertionsort(array, size);
                printf("Time Taken: %f in seconds\n", duration);
                break;
        case 4: duration=time_mergesort(array, size);
                printf("Time Taken: %f in seconds\n", duration);
                break;
        case 5: duration=time_quicksort(array, 0, size-1);
                printf("Time Taken: %f in seconds\n", duration);
                break;
        case 6: system("clear");
                display(array,size);
                mainmenu(array, size);
                break;
        case 7: system("clear");
                testall(array, size);
                break;
        case 8: printf("Enter new length:");
                scanf("%d", &size);
                array= generate(size);
                system("clear");
                mainmenu(array, size);
                free(array);
                break;
        }
}

/*-----------------------------------------------
            Display elements in array
-------------------------------------------------*/
void display(int *array, int length){
    int i;
    printf("\nContent: ");
    for (i = 0; i < length; i++) {
        printf("%d  ",array[i]);
    }
    printf("\n");
}

/*-----------------------------------------------
   creates & Populates array with random numbers
-------------------------------------------------*/
int* generate(int max){
    int i, *array;
    array=(int *)malloc(max*sizeof(int));

    srand(time(NULL));
    for(i=0; i<max; i++){
        array[i]=rand()%max;
    }
    return array;
}

/*-----------------------------------------------
        Menu information to show on screen
-------------------------------------------------*/
int showmenu(int size){
    int input;
    printf("\n-------------------------------------------\n");
    printf("Current array length: %d\n\n", size);
    printf("Enter 1 to run Bubble Sort Algorithm\n");
    printf("Enter 2 to run Selection Sort Algorithm\n");
    printf("Enter 3 to run Insertion Sort Algorithm\n");
    printf("Enter 4 to run Merge Sort Algorithm\n");
    printf("Enter 5 to run Quick Sort Algorithm\n");
    printf("Enter 6 to show unsorted array\n");
    printf("Enter 7 to test all Algorithms\n");
    printf("Enter 8 to change array length\n");
    printf("-------------------------------------------\n");
    printf("\nSelect an option or 0 to quit: ");
    scanf("%d",&input );

    return input;
}
