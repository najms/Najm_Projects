#include "result.h"
#include "functions.h"

void runproject();
int size=10000;
bool control=true, project=false;
int main(int argc, char* args[]){
    init();
    project=render_homepage();
    if(project==true){
        control=false;
    }

    runproject();
    leave();
    return 0;
}

/*---------------------------------------------------------
runs one of the projects, if the user wants sorting algorithms
instead, then SDL is closed and sorting algorithms is started
---------------------------------------------------------*/
void runproject(){
    while(control){
        render_getreadypage();
        playgame();
        control=showresults();
    }
    if(project==true && control==false){
        closesdl();
        int *array=generate(size);
        system("clear");
        mainmenu(array, size);
        free(array);
    }
}
