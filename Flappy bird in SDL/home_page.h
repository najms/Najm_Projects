#include "sdl_functions.h"

extern SDL_Texture* ground;
extern SDL_Texture* background;
extern SDL_Texture* birdtexture;
extern SDL_Texture* title;
extern SDL_Texture* playbtn;
extern SDL_Texture* sortbtn;
extern bool shutdown;

bool render_homepage();
void showbackground(bool move);
void showground(bool move);
void showbird(bool move, bool rendcopy);
void movebird_updown();
void showtitle();
void loadmedia();
void mousepressed();
void sorting_algorithm();
void fadehome(bool stop);
bool transition();
