#include "sdl_functions.h"

SDL_Window* Window=NULL;
SDL_Renderer* render=NULL;
SDL_Surface* temp=NULL;
/*---------------------------------------------------------
                    Initiliasing SDL
---------------------------------------------------------*/
void init(){
    SDL_Init(SDL_INIT_EVERYTHING);
    TTF_Init();
    Window=SDL_CreateWindow("Kabuli Bird", 250,150, 288,512,SDL_WINDOW_SHOWN);

    render=SDL_CreateRenderer(Window,-1,SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(render,0xFF,0xFF,0xFF,0xFF);
}

/*---------------------------------------------------------
             checks if two objects collide
---------------------------------------------------------*/
bool checkCollision(SDL_Rect a, SDL_Rect b){
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;
    //extracts the position of the objs from the rects
    leftA=a.x;
    rightA=a.x+a.w;
    topA=a.y;
    bottomA=a.y+a.h;
    leftB=b.x;
    rightB=b.x+b.w;
    topB=b.y;
    bottomB=b.y+b.h;

    //checks the x and y co ordinates of both objects
    if(bottomA<=topB)    return false;
    if(topA>=bottomB)    return false;
    if(rightA<=leftB)    return false;
    if(leftA>=rightB)    return false;
    return true;
}

/*---------------------------------------------------------
shows a message on screen and closes the window. this is
only used and shown if the user wants to use the sorting
algorithm instead of playing the game.
---------------------------------------------------------*/
void closesdl(){
    SDL_Rect msg={31,130,226,311};
    SDL_Rect bckgrnd={0,0,288,512};
    bool quit=false;
    //load necessary textures
    temp=IMG_Load("graphics/double-back.png");
    SDL_Texture* background2=SDL_CreateTextureFromSurface(render,temp);
    temp=IMG_Load("graphics/message.png");
    SDL_Texture* message=SDL_CreateTextureFromSurface(render,temp);
    SDL_RenderCopy(render, background2,&bckgrnd,NULL);
    SDL_RenderCopy(render, message,NULL,&msg);

    int starttime=SDL_GetTicks();
    int duration;
    while(quit!=true){
        duration=(SDL_GetTicks()-starttime)/1000;
        if(duration==4){
            quit=true;
        }
        SDL_RenderPresent(render);
    }
    //free textures
    SDL_DestroyTexture(message);
    SDL_DestroyTexture(background2);
    SDL_DestroyWindow(Window);
}
