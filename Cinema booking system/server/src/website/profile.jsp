<!doctype html>
<%@ page import="server.Main,server.database.*" %>
<%
if (Main.connection.check(request, response, true)) {
%>
<%
Customers customer = null;
Staff staff = null;
if ((String)request.getSession().getAttribute("user") == "customer") {
  customer = (Customers)request.getSession().getAttribute("customer");
} else {
  staff = (Staff)request.getSession().getAttribute("staff");
}
%>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />

  <title>Profile</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet"href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Voltaire|Roboto|Overpass" rel="stylesheet">
  <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/muv.css" rel="stylesheet"/>




</head>

<body class="profile-page">
  <nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
      <!-- pand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a href="index.jsp"class="logo-container">
          <img src="images/logo.png" class="logo-container" id="logo-container">
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse site-navigation-row" id="navigation">

        <ul class="nav navbar-nav navbar-right">
          <li>
              <a href="whatson.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">WHAT'S ON</h2>
            </a>
          </li>
          <li>
            <a href="offers.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">OFFERS</h2>
            </a>
          </li>

          <li>
            <a href="vip.jsp" class=" link is-notactive mdl-navigation__link">
              <h2 class="nav-title">VIP</h2>
            </a>
          </li>
          <li>
            <a href="info.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">INFORMATION</h2>
            </a>
          </li>
          <li class="nav-item">
              <a href="profile.jsp" class="nav-link profile">
                  <i class="material-icons profile-container2">account_circle</i>
                  <h6>Profile</h6>
              </a>
          </li>
          <% if (Main.login.checkLogin(request)) { %>
          <li class="nav-item">
              <a href="signOut.jsp" class="nav-link profile">
                  <i class="material-icons join-container2">event_seat</i>
                  <h6>Log Out</h6>
              </a>
          </li>
          <%} else { %>
          <li class="nav-item">
              <a href="Login.jsp" class="nav-link profile">
                  <i class="material-icons join-container2">event_seat</i>
                  <h6>sign in</h6>
              </a>
          </li>
          <% } %>
          <li>
            <form action="search.jsp" method="get">
              <input type="text" class="input-lg input search" name="search" placeholder="Search...">
            </form>
            </li>
        </ul>
      </div>
    </div>
  </nav>
    <a class="btn pulse cd-top" href="#0" title="Back to top" id="myBtn">
    <i class="material-icons">arrow_upward</i></a>
    <div class="main profile">
      <div class="container">
        <div class="section text-center section-index">
          <div class="row">
            <div class="col-md-6 col-md-offset-2">
            </div>
          </div>

                <div class="overview-meta features">
                    <div>
                          <div class="dashboard display-animation" style="margin: auto; width: auto;">
                            <img src="images/profile_icon.jpg" alt="Profile icon" style="width:100%">
                            </div>
                        <hr>
                            <dl class="display-animation">
                                <dt>Account Holder</dt>
                                <dd><%= (customer == null) ? staff.getUsername() : (customer.getName() + " " + customer.getSurname()) %></dd>
                            </dl>
                                <dl class="display-animation">
                                    <dt>Membership Type</dt>
                                    <%if(staff != null){%>
                                    <dd>STAFF</dd>
                                    <%} else if (customer != null){%>
                                    <dd>GOLD</dd>
                                    <% } else{%>
                                    <dd>Unknown</dd>
                                    <% } %>
                                </dl>
                            <dl class="display-animation">
                                <dt>Purchases</dt>
                                <%if(staff != null){%>
                                <dd>N/A (Staff Member)</dd>
                                <%} else if (customer != null){%>
                                <dd>0</dd>
                                <% } else{%>
                                <dd>Unknown</dd>
                                <% } %>
                            </dl>
                            <dl class="display-animation">
                                <!-- <dt>Last login</dt>
                                <dd>Thursday, 19 April 2018</dd> -->
                            </dl>
                        </div>
                    </div>
                <div class="container">
                  <h1 class="detail-title profile">Your Account</h1>
                  <%if(staff != null){%>
                  <dd>Staff Account.</dd>
                  <%} else if (customer != null){%>
                  <dd>  <div><p class="p2">
                      This is your Account. You have the Highest membership offered at MUV, this enables you to watch unlimited movies every month, 15% off all food and drinks bought from MUV aswell as small local food businesses. aswell as this you are entitled to use
                    MUV's exclusive VIP seats with family and friends</p>
                    </div>
                    <button type="button" class="btn2  buttonG" data-toggle="collapse" data-target="#rm">Additional information</button>
                    <div id="rm" class="collapse">
                      <div><p class="p2">Cookies are small text files that are placed on your computer by websites that you visit. They are widely used in order to make websites work, or work more efficiently, as well as to provide information to the owners of the site. </p>
                      <p class="p2">if you have any queries or requests at any time concerning your personal information held by us or our practices in this regard, please write to us on our email customerservices@MUVmail.com.</p>
                      <p class="p2">You Can Redeem or upgrade your memebership  <a href="vip.jsp" class=" link is-notactive mdl-navigation__link"><u>here</u></a></p>
                    </div>
                  </div></dd>
                  <% } else{%>
                  <dd>Unknown</dd>
                  <% } %>
</div>
          <footer class="panel-footer" role="contentinfo">
          <div>
              <section class="container">
                  <h2 class="title2">
                      Customer service

                      <a href="tel:0121 211 4620"></a>
                            <span class="footer-title">0121 211 4620</span>
                  </h2>
                  <p>Call will be charged at your local rate.</p>

                  <div class="title2">SOCIAL</div>
                  <ul>
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-youtube"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                  </ul>
                  <div class="title2">GET IN TOUCH</div>
                  <ul>
                    <li><a href="info.jsp" class="footer-title">FAQ</a></li>
                    <li><a href="info.jsp" class="footer-title">Contact us</a></li>
                    <li><a href="info.jsp" class="footer-title">Feedback</a></li>
                    </ul>
                    <div class="title2">LEGAL</div>
                    <ul>
                      <li><a href="info.jsp" class="footer-title">About us</a></li>
                      <li><a href="info.jsp" class="footer-title">Terms & Conditions</a></li>
                      <li><a href="info.jsp" class="footer-title">Privacy</a></li>
                      <li><a href="info.jsp" class="footer-title">Accessability</a></li>
                      </ul>
                              </section>

                              <div>
                              <section href="index.jsp"class="container ">
                                <img src="images/logo.png" class="logo-footer container">
                              </section>
                            </div>
                              <hr class="side">
              <div>
                  <section class="container ">
                      <p> &#169; MUV. All rights reserved. </p>
                  </section>
              </div>
            </footer>


                <!--   Core JS Files   -->
                <script src="assets/js/jquery.min.js" type="text/javascript"></script>
                <!-- <script src="assets/js/bootstrap.min.js" type="text/javascript"></script> -->
                <script src="assets/js/counter.min.js" type="text/javascript"></script>
                <script src="assets/js/headingsindex.min.js" type="text/javascript"></script>
                <script src="assets/js/material.min.js"></script>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                <script src="js/scrollreveal.min.js"></script>
                <script src="http://code.jquery.com/jquery.js"></script>
                <script src="assets/js/jquery.waypoints.waypoints.min.js" type="text/javascript"></script>

                <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
                <script src="assets/js/material-kit.js" type="text/javascript"></script>
                <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

                <% } else {
                response.sendRedirect("/Login.jsp");
                } %>
