<!doctype html>
<%@ page import="server.Main,server.database.*,java.util.*" %>
<% if (Main.connection.check(request, response, false)) {
List<Movies> movies = Main.database.queryMovies();
%>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>MUV Offers</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Voltaire|Roboto|Overpass" rel="stylesheet">
    <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/muv.css" rel="stylesheet"/>
  <link href="assets/css/Login.css" rel="stylesheet"/>
  <!-- <link href="assets/css/logo-container.css" rel="stylesheet"/> -->
  <link rel="stylesheet" href="assets/css/material-photo-gallery.css">


</head>

<body class="profile-page">


  <nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a href="index.jsp"class="logo-container">
          <img src="images/logo.png" class="logo-container" id="logo-container">
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>


      <div class="collapse navbar-collapse site-navigation-row" id="navigation">

        <ul class="nav navbar-nav navbar-right">
          <li>
              <a href="whatson.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">WHAT'S ON</h2>
            </a>
          </li>
          <li>
            <a href="offers.jsp" class="link active mdl-navigation__link">
              <h2 class="nav-title">OFFERS</h2>
            </a>
          </li>

          <li>
            <a href="vip.jsp" class=" link is-notactive mdl-navigation__link">
              <h2 class="nav-title">VIP</h2>
            </a>
          </li>
          <li>
            <a href="info.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">INFORMATION</h2>
            </a>
          </li>
          <li class="nav-item">
              <a href="profile.jsp" class="nav-link profile">
                  <i class="material-icons profile-container2">account_circle</i>
                  <h6>Profile</h6>
              </a>
          </li>
          <% if (Main.login.checkLogin(request)) { %>
          <li class="nav-item">
              <a href="signOut.jsp" class="nav-link profile">
                  <i class="material-icons join-container2">event_seat</i>
                  <h6>Log Out</h6>
              </a>
          </li>
          <%} else { %>
          <li class="nav-item">
              <a href="Login.jsp" class="nav-link profile">
                  <i class="material-icons join-container2">event_seat</i>
                  <h6>sign in</h6>
              </a>
          </li>
          <% } %>
          <li>
            <form action="search.jsp" method="get">
              <input type="text" class="input-lg input search" name="search" placeholder="Search...">
            </form>
            </li>
        </ul>
      </div>
    </div>
  </nav>
<div>

    <a class="btn pulse cd-top" href="#0" title="Back to top" id="myBtn">
    <i class="material-icons">arrow_upward</i></a>
    <div class="wrapper">
      <div class="header4">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="unbreakable-container">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="main">
                        <div class="offerBanners">
                        <div class="wrapper">
                          <div class="item header2" style="background-image: url('images/offer2.jpg')" style="width:100%;">
                            <div class="container image-hover">
                              <div class="row">
                                <div class="col-md-6">
                                <div class="container">
                                  <div class="BannerContentLeft">
                                    <div class="row">
                                      <div class="col"><h1 class="OfferTitle">EXPERIENCE THE BEST WITH MUV's LIMITED TIME OFFERS</h1></div>
                                        <div class="col">
                                          <p class="OfferText">Enjoy these amazing deals with both friends and family,</p> <p class="OfferText">while you save!</p><br>
                                          <div class="animation-element bounce-up cf">
                                            <div class="subject development">

                                              <a type="button" onclick="limitedDeals()" class="btn button1 buttonG">SHOW ME WHAT'S ON</a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div id="limitedDeals" onclick="limitedDeals()"style="display:none;">
                          <div class="container display-animation">
                          <h2 class="offersTitle">Catch them while they last!</h2>
                          <div class="row">
                            <% int k = 0;
                             for (int i = 0; i < movies.size() && k < 4; i++) {
                               if (movies.get(i).isOffer()) {
                                 k++;
                                 String poster = null;
                                 List<Images> images = Main.database.getImages(movies.get(i).getMovieId());
                                 for (Images image : images) {
                                   if (image.getImageId().substring(0, image.getImageId().length() - 4).toLowerCase().endsWith("poster"))
                                     poster = image.getImageId();
                                 }
                              %>
                            <div class="col-md-3">
                                <div class="img-thumbnail button1 buttonB">
                                  <a href="details.jsp" target="_self" class="link-movie">
                                    <img src="images/<%= (poster == null) ? "logo.png" : poster %>" alt="<%= movies.get(i).getName() %>" style="width:100%">
                                    <div class="caption">
                                        <h3><%= movies.get(i).getName() %></h3>
                                    </div>
                                  </a>
                                </div>
                              </div>
                              <%
                                }
                              }
                              %>
                          </div>
                        </div>
                      </div>
                        <div class="offerBanners">
                        <div class="wrapper">
                          <div class="header2" style="background-image: url('images/offer1.jpg');">
                            <div class="container image-hover">
                              <div class="row">
                              <div class="BannerContentRight">
                                <div class="row">
                                  <div class="col"><h1 class="OfferTitle2">GET A GREAT DEAL ON THE BEST FAMILY FILMS</h1></div>
                                    <div class="col">
                                      <p class="OfferText2">Bring the family along for a film experience they will never</p> <p class="OfferText2">forget with our Family Ticket.</p>
                                      <div class="animation-element bounce-up cf">
                                        <div class="subject development">

                                          <a type="button" onclick="familyDeals()"class="btn button1 buttonG BannerContentRight2 ">SHOW ME WHAT'S ON</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        </div>
                        <div id="familyDeals" onclick="familyDeals()"style="display:none;">
                          <div class="container display-animation">
                          <h2 class="offersTitle">No coupon needed, Just ask for the family deal at our MUV Cinema!</h2>
                          <div class="row">
                            <% int j = 0;
                             for (int i = 0; i < movies.size() && j < 4; i++) {
                               if (movies.get(i).isFamily()) {
                                 j++;
                                 String poster = null;
                                 List<Images> images = Main.database.getImages(movies.get(i).getMovieId());
                                 for (Images image : images) {
                                   if (image.getImageId().substring(0, image.getImageId().length() - 4).toLowerCase().endsWith("poster"))
                                     poster = image.getImageId();
                                 }
                              %>
                            <div class="col-md-3">
                                <div class="img-thumbnail button1 buttonB">
                                  <a href="details.jsp" target="_self" class="link-movie">
                                    <img src="images/<%= (poster == null) ? "logo.png" : poster %>" alt="<%= movies.get(i).getName() %>" style="width:100%">
                                    <div class="caption">
                                        <h3><%= movies.get(i).getName() %></h3>
                                    </div>
                                  </a>
                                </div>
                              </div>
                              <%
                                }
                              }
                              %>

                          </div>
                        </div>
                      </div>

                        <div class="offerBanners">
                        <div class="wrapper">
                          <div class="item header2" style="background-image: url('images/offer3.jpg')" style="width:100%;">
                            <div class="container image-hover">
                              <div class="row">
                                <div class="col-md-6">
                                <div class="container">
                                  <div class="BannerContentLeft">
                                    <div class="row">
                                      <div class="col"><h1 class="OfferTitle">GET A GREAT STUDENT DEAL FROM US AT MUV</h1></div>
                                        <div class="col">
                                          <p class="OfferText">Catch a breath from work and let MUV spoil you and your friends</p> <p class="OfferText"> with this fabulous offer.</p>
                                          <div class="animation-element bounce-up cf">
                                            <div class="subject development">
                                              <a type="button" onclick="studentDeals()" class="btn button1 buttonG">SHOW ME WHAT'S ON</a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div id="studentDeals" onclick="studentDeals()" style="display:none;">
                          <div class="container display-animation">
                          <h2 class="offersTitle">Bring along your student card and you're set!</h2>
                          <div class="row">
                            <% int x = 0;
                             for (int i = 0; i < movies.size() && x < 4; i++) {
                               if (movies.get(i).isStudent()) {
                                 x++;
                                 String poster = null;
                                 List<Images> images = Main.database.getImages(movies.get(i).getMovieId());
                                 for (Images image : images) {
                                   if (image.getImageId().substring(0, image.getImageId().length() - 4).toLowerCase().endsWith("poster"))
                                     poster = image.getImageId();
                                 }
                              %>
                            <div class="col-md-3">
                                <div class="img-thumbnail button1 buttonB">
                                  <a href="details.jsp" target="_self" class="link-movie">
                                    <img src="images/<%= (poster == null) ? "logo.png" : poster %>" alt="<%= movies.get(i).getName() %>" style="width:100%">
                                    <div class="caption">
                                        <h3><%= movies.get(i).getName() %></h3>
                                    </div>
                                  </a>
                                </div>
                              </div>
                              <%
                                }
                              }
                              %>
                          </div>
                        </div>
                      </div>


                      <div class="offerBanners">
                      <div class="wrapper">
                        <div class="header2" style="background-image: url('images/offer4.jpg');">
                          <div class="container image-hover">
                            <div class="row">
                            <div class="BannerContentRight">
                              <div class="row">
                                <div class="col"><h1 class="OfferTitle2">GET A GREAT DEAL ON THE BEST CLASSIC FILMS</h1></div>
                                  <div class="col">
                                    <p class="OfferText2">Dont miss out on great deals</p> <p class="OfferText2">Grab yourself a MUV ticket to enjoy these classic movies!.</p>
                                    <div class="animation-element bounce-up cf">
                                      <div class="subject development">
                                        <a type="button" onclick="classicDeals()"class="btn button1 buttonG BannerContentRight2">SHOW ME WHAT'S ON</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                      <div id="classicDeals" onclick="classicDeals()" style="display:none;">
                        <div class="container display-animation">
                        <h2 class="offersTitle">Buy your ticket now to get a % off these amazing films</h2>
                        <div class="row">
                          <% int y = 0;
                           for (int i = 0; i < movies.size() && y < 4; i++) {
                             if (movies.get(i).isClassic()) {
                               y++;
                               String poster = null;
                               List<Images> images = Main.database.getImages(movies.get(i).getMovieId());
                               for (Images image : images) {
                                 if (image.getImageId().substring(0, image.getImageId().length() - 4).toLowerCase().endsWith("poster"))
                                   poster = image.getImageId();
                               }
                            %>
                          <div class="col-md-3">
                              <div class="img-thumbnail button1 buttonB">
                                <a href="details.jsp" target="_self" class="link-movie">
                                  <img src="images/<%= (poster == null) ? "logo.png" : poster %>" alt="<%= movies.get(i).getName() %>" style="width:100%">
                                  <div class="caption">
                                      <h3><%= movies.get(i).getName() %></h3>
                                  </div>
                                </a>
                              </div>
                            </div>
                            <%
                              }
                            }
                            %>
                              </div>
                            </div>
                          </div>




<div class="text-center">

                        <footer class="panel-footer" role="contentinfo">

                        <div>
                            <section class="container">
                                <h2 class="title2">
                                    Customer service

                                    <a href="tel:0121 211 4620"></a>
                                          <span class="footer-title">0121 211 4620</span>
                                </h2>
                                <p>Call will be charged at your local rate.</p>

                                <div class="title2">SOCIAL</div>
                                <ul>
                                  <li><a href="#" class="fa fa-facebook"></a></li>
                                  <li><a href="#" class="fa fa-twitter"></a></li>
                                  <li><a href="#" class="fa fa-youtube"></a></li>
                                  <li><a href="#" class="fa fa-instagram"></a></li>
                                </ul>
                                <div class="title2">GET IN TOUCH</div>
                                <ul>
                                  <li><a href="info.jsp" class="footer-title">FAQ</a></li>
                                  <li><a href="info.jsp" class="footer-title">Contact us</a></li>
                                  <li><a href="info.jsp" class="footer-title">Feedback</a></li>
                                  </ul>
                                  <div class="title2">LEGAL</div>
                                  <ul>
                                    <li><a href="info.jsp" class="footer-title">About us</a></li>
                                    <li><a href="info.jsp" class="footer-title">Terms & Conditions</a></li>
                                    <li><a href="info.jsp" class="footer-title">Privacy</a></li>
                                    <li><a href="info.jsp" class="footer-title">Accessability</a></li>
                                    </ul>
                                            </section>

                                            <div>
                                            <section href="index.jsp"class="container ">
                                              <img src="images/logo.png" class="logo-footer container">
                                            </section>
                                          </div>



                                            <hr class="side">




                            <div>
                                <section class="container ">
                                    <p> &#169; MUV. All rights reserved. </p>
                                </section>
                            </div>
                          </div>
                        </div>
                          </footer>







                              <!--   Core JS Files   -->
                              <script src="assets/js/showMe.js" type="text/javascript"></script>
                              <script src="assets/js/jquery.min.js" type="text/javascript"></script>
                              <!-- <script src="assets/js/bootstrap.min.js" type="text/javascript"></script> -->
                              <script src="assets/js/counter.min.js" type="text/javascript"></script>
                              <script src="assets/js/headingsindex.min.js" type="text/javascript"></script>
                              <script src="assets/js/material.min.js"></script>
                              <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                              <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                              <script src="js/scrollreveal.min.js"></script>
                              <script src="http://code.jquery.com/jquery.js"></script>
                              <script src="assets/js/jquery.waypoints.waypoints.min.js" type="text/javascript"></script>

                              <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
                              <script src="assets/js/material-kit.js" type="text/javascript"></script>
                              <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

                              <% } else {
                              response.sendRedirect("/index.jsp");
                              } %>
