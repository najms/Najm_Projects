<!doctype html>
<%@ page import="server.Main,server.database.*,java.text.SimpleDateFormat,java.util.*" %>
<%
if (Main.connection.check(request, response, false)) {
Customers customer = null;
Staff staff = null;
if ((String)request.getSession().getAttribute("user") == "customer") {
  customer = (Customers)request.getSession().getAttribute("customer");
} else {
  staff = (Staff)request.getSession().getAttribute("staff");
}
String timeId = request.getParameter("timeId");
boolean redirect = false;
if (timeId != null && !redirect) {
  TimeSlots slot = Main.database.getTimeslot(Integer.parseInt(timeId));
  if (slot != null && !redirect) {
    Movies movie = Main.database.getMovie(slot.getTimeslot_movieId());
    String errorMessage = "";
    boolean pdf = false;
    if (request.getParameter("payButton") != null && !redirect) {
      errorMessage = Main.database.checkTicketForm(request);
      if (!errorMessage.startsWith("Error")) {
        pdf = true;
      }
    }
%>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Payment</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Voltaire|Roboto|Overpass" rel="stylesheet">
    <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/muv.css" rel="stylesheet"/>
  <link href="assets/css/Login.css" rel="stylesheet"/>
  <!-- <link href="assets/css/logo-container.css" rel="stylesheet"/> -->
  <link rel="stylesheet" href="assets/css/material-photo-gallery.css">

</head>

<body class="pay-page">
  <nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a href="index.jsp"class="logo-container">
          <img src="images/logo.png" class="logo-container" id="logo-container">
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse site-navigation-row" id="navigation">
        <ul class="nav navbar-nav navbar-right">
          <li class="nav-item">
              <a href="Login.jsp" class="nav-link profile">
                  <img class="profile-container3" src="images/account.png">

              </a>
          </li>
          <li>
            <form action="search.jsp" method="get">
              <input type="text" class="input-lg input search" name="search" placeholder="Search...">
            </form>
            </li>
        </ul>
      </div>
    </div>
  </nav>
    <a class="btn pulse cd-top" href="#0" title="Back to top" id="myBtn">
    <i class="material-icons">arrow_upward</i></a>
    <div class="wrapper">
      <div class="header2" style="background-image: url('images/pay.jpg');">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="container">
                <div class="unbreakable-container">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="main">
      <div class="container">
        <div class="section text-center section-index">
          <div class="row">
            <div class="col-md-6 col-md-offset-2">
            </div>
          </div>
          <div class="animation-element bounce-up cf">
              <div class="subject development">
                              <div class="overview-meta features">
                                  <div>
                                    <%
                                    String poster = null;
                                     List<Images> images = Main.database.getImages(movie.getMovieId());
                                      for (Images image : images) {
                                        String id = image.getImageId().substring(0, image.getImageId().length() - 4).toLowerCase();
                                      if (id.endsWith("poster")) {
                                          poster = image.getImageId();
                                        }
                                      }%>
                                        <div class="dashboard display-animation" style="margin: auto; width: auto;">
                                          <img src="images/<%=poster%>" alt="Image Poster" style="width:100%">
                                          </div>
                                      <hr>
                                          <dl class="display-animation">
                                              <dt>Date & Time</dt>
                                              <% String dateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(slot.getStartTime().getTime()); %>
                                              <dd><%= dateTime %></dd>
                                          </dl>
                                              <dl class="display-animation">
                                                  <dt>Running Time</dt>
                                                  <% String runningTime = new SimpleDateFormat("H:mm").format(movie.getDuration().getTime()); %>
                                                  <dd><%= runningTime %> hrs</dd>
                                              </dl>
                                          <dl class="display-animation">
                                              <dt>Where</dt>
                                              <dd>MUV Leeds The Light</dd>
                                          </dl>
                                          <dl class="display-animation">
                                              <dt>Screen</dt>
                                              <dd><%= slot.getScreen() %></dd>
                                          </dl>
                                      </div>
                                  </div>
                    <div class="row">
                      <div class="container">
                        <h2 class="detail-title2">NOW BOOKING</h2>
                        <h1 class="detail-title"><%= movie.getName()%></h1>
                        <h5><%= movie.getCertificate()%></h5>
                        <div>
                          <div class="col-xs-12 col-md-8">
                              <div class="panel panel-default credit-card-box">
                                  <div class="panel-heading display-table" >
                                      <div class="row display-tr" >
                                          <h2 class="panel-title display-td" >Payment Details</h2>
                                          <div class="display-td" >
                                              <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="panel-body">
                                    <% if (errorMessage != "") { %>
                                      <label class="error title3"><%= errorMessage %></label>
                                    <% } %>
                                    <form action="" method="post">
                                      <div class="form-row">
                                      <div class="form-group  profile col-md-12">
                                        <input type="txt" class="loginanimation" id="name" placeholder="" name="name" value="<%= (customer == null) ? ((staff == null) ? "" : staff.getUsername()) : (customer.getName() + " " + customer.getSurname()) %>" required autocomplete="off">
                                          <label class="" for="name"><span>Name On Card</span></label>
                                        </div>
                                      </div>
                                      <div class="form-row">
                                        <div class="form-group profile col-md-12">
                                          <input type="cc" onkeyup="checkCard()" onkeypress="return (checkDigit(event) && checkCardLength(event))" class="loginanimation" id="cardnumber" placeholder="" name="cardnumber" value="<%= (customer == null) ? "" : ((customer.getCardNumber() == null) ? "" : customer.getCardNumber()) %>" required autocomplete="off"<%= (customer == null) ? "" : ((customer.getCardNumber() == null) ? "" : " onkeydown=\"return false;\"") %>>
                                            <label class="" for="cardnumber"><span>Card Number</span></label>
                                          </div>
                                          <% if (customer != null && customer.getCardNumber() != null) { %>
                                              <div class="form-group profile">
                                                <div class="col-sm-offset-0 col-sm-2">
                                                  <button id="cardButton" name="cardButton" onclick="cardShow()" type="button" class="btn btn-success">Change Card</button></br>
                                                </div>
                                              </div>
                                            <% } %>
                                          </div>
                                          <div class="form-row">
                                            <div class="form-group  profile col-md-12">
                                              <input type="txt" onkeyup="validateEmail()" class="loginanimation" id="email" placeholder="" name="email" required autocomplete="off" value="<%= (customer == null) ? ((staff == null) ? "" : staff.getUsername()) : (customer.getEmail()) %>">
                                                <label class="" for="email"><span id="submitEmailMessage" class="submitMessage">Email</span></label>
                                              </div>
                                              </div>
                                            <div id="payRow" class="form-row collapse<%= (customer == null) ? " in" : ((customer.getCardNumber() == null) ? " in" : "") %>">
                                              <div class="form-group  profile col-md-4">
                                                <input type="cc" class="loginanimation" id="cvc" onkeypress="return (checkDigit(event) && checkCvcLength(event))"  placeholder="" name="cvc" required autocomplete="off" value="<%= (customer == null) ? "" : ((customer.getCardNumber() == null) ? "" : "111") %>">
                                                  <label class="" for="cvc"><span>CVC</span></label>
                                                </div>
                                                <div class="form-group  profile col-md-4">
                                                  <input type="cc" onkeyup="validateMonth()" class="loginanimation" id="expirymonth" onkeypress="return (checkDigit(event) && checkMonthLength(event))" placeholder="" name="expirymonth" required autocomplete="off" value="<%= (customer == null) ? "" : ((customer.getCardNumber() == null) ? "" : "11") %>">
                                                    <label class="" for="expirymonth"><span id="submitMonthMessage" class="submitMessage">Expiry Month</span></label>
                                                  </div>
                                                  <div class="form-group  profile col-md-4">
                                                    <input type="cc" class="loginanimation" id="expiryyear" onkeypress="return (checkDigit(event) && checkYearLength(event))" placeholder="" name="expiryyear" required autocomplete="off" value="<%= (customer == null) ? "" : ((customer.getCardNumber() == null) ? "" : "11") %>">
                                                      <label class="" for="expiryyear"><span>Expiry Year</span></label>
                                                    </div>
                                                </div>

                                                  <div class="form-row">
                                                    <div class="form-group  profile col-md-4">
                                                      <input type="int" class="loginanimation" id="numofchildren" placeholder="" value="0" onkeypress="return (checkDigit(event) && checkNumLength(event))"  onkeyup="Price()"name="numofchildren" required autocomplete="off">
                                                        <label class="" for="numofchildren"><span># of Children</span></label>
                                                      </div>

                                                    <div class="form-group  profile col-md-4">
                                                      <input type="int" class="loginanimation" id="numofadults" placeholder="" value="0" onkeypress="return (checkDigit(event) && checkNumLength(event))" onkeyup="Price()" name="numofadults" required autocomplete="off">
                                                        <label class="" for="numofadults"><span># of adults</span></label>
                                                      </div>
                                                        <div class="form-group  profile col-md-4">
                                                          <input type="int" class="loginanimation" id="numofseniors" placeholder="" value="0" onkeypress="return (checkDigit(event) && checkNumLength(event))" onkeyup="Price()"name="numofseniors" required autocomplete="off">
                                                            <label class="" for="numofseniors"><span># of Seniors</span></label>
                                                          </div>
                                                    </div>
                                                    <div class="form-row">
                                                      <div class="form-check col-md-12">
                                                        <label class="form-check-label checkboxVip" style="display: ruby;"><span id="vipMessage" class="vipMessage">Add VIP Seats</span>
                                                          <input type="checkbox" onclick="showSeating()" class="form-check-input" id="vipSeating" name="vipSeating">
                                                          </label>
                                                      </div>
                                                      </div>
                                                    <div id="vipSeats" class="row col-md-12" style="display:none;">
                                                      <h2>VIP Seating Plan</h2>
                                                      <form id="reservation" method="post">
                                                        <div class="row">
                                                          <div class="form-group col-md-12">
                                                          <%
                                                          List<Seats> seats = Main.database.getSeats(Integer.parseInt(timeId));
                                                          for (int i = seats.size() - 1; i >= 0; i--) {
                                                            if (!seats.get(i).getVipSeatsFlag()) {
                                                              seats.remove(i);
                                                            }
                                                          }

                                                          for (int i = 1; i < seats.size() + 1; i++) {
                                                          %>
                                                          <input id="seat-<%= i %>" class="seat-select<%= seats.get(i - 1).getSeatTakenFlag() ? "-taken\" onclick=\"return false;\"" : "" %>" type="checkbox" value="<%= i %>" name="seat" />
                                                          <label for="seat-<%= i %>" class="seat"></label>
                                                            <%
                                                            if (i % 10 == 0) {
                                                            %>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <div class="form-group col-md-12">
                                                          <%
                                                            }
                                                          }
                                                          %>
                                                        </div>
                                                        </div>
                                                        </div>
                                                      <div class="form-row">
                                                          <div class="form-group profile">
                                                            <div class="col-sm-offset-1 col-sm-10">
                                                              <button id="payButton" name="payButton" type="submit" class="btn btn-success"><span id="submitMessage" class="submitMessage">Pay: 0</span></button></br>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <input type="hidden" name="timeid" value="<%= timeId %>">
                                                        </form>

                                                      </div>
                                  </div>
                              </div>
                        </div>
                      </div>


</div>

</div>

    <div class="text-center">

                <footer class="panel-footer" role="contentinfo">


                <div>
                    <section class="container">
                        <h2 class="title2">
                            Customer service
                            <a href="tel:0121 211 4620"></a>
                                  <span class="footer-title">0121 211 4620</span>
                        </h2>
                        <p>Call will be charged at your local rate.</p>

                        <div class="title2">SOCIAL</div>
                        <ul>
                          <li><a href="#" class="fa fa-facebook"></a></li>
                          <li><a href="#" class="fa fa-twitter"></a></li>
                          <li><a href="#" class="fa fa-youtube"></a></li>
                          <li><a href="#" class="fa fa-instagram"></a></li>
                        </ul>
                        <div class="title2">GET IN TOUCH</div>
                        <ul>
                          <li><a href="info.jsp" class="footer-title">FAQ</a></li>
                          <li><a href="info.jsp" class="footer-title">Contact us</a></li>
                          <li><a href="info.jsp" class="footer-title">Feedback</a></li>
                          </ul>
                          <div class="title2">LEGAL</div>
                          <ul>
                            <li><a href="info.jsp" class="footer-title">About us</a></li>
                            <li><a href="info.jsp" class="footer-title">Terms & Conditions</a></li>
                            <li><a href="info.jsp" class="footer-title">Privacy</a></li>
                            <li><a href="info.jsp" class="footer-title">Accessability</a></li>
                            </ul>
                                    </section>

                                    <div>
                                    <section href="index.jsp"class="container card-footer">
                                      <img src="images/logo.png" class="logo-footer container">
                                    </section>
                                  </div>



                                    <hr class="side">

                    <div>
                        <section class="container card-footer">
                            <p> &#169; MUV. All rights reserved. </p>
                        </section>
                    </div>
                  </footer>

                      <!--   Core JS Files   -->
                      <script src="assets/js/jquery.min.js" type="text/javascript"></script>
                      <script src="assets/js/payment.js" type="text/javascript"></script>
                      <!-- <script src="assets/js/bootstrap.min.js" type="text/javascript"></script> -->
                      <script src="assets/js/counter.min.js" type="text/javascript"></script>
                      <script src="assets/js/headingsindex.min.js" type="text/javascript"></script>
                      <script src="assets/js/material.min.js"></script>
                      <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                      <script src="js/scrollreveal.min.js"></script>
                      <script src="http://code.jquery.com/jquery.js"></script>
                      <script src="assets/js/jquery.waypoints.waypoints.min.js" type="text/javascript"></script>

                      <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
                      <script src="assets/js/material-kit.js" type="text/javascript"></script>
                      <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

<%
        if (pdf) {
          response.sendRedirect("/tickets/" + errorMessage);
        }
      } else {
    redirect = true;
  }
} else {
  redirect = true;
}
if (redirect)
  response.sendRedirect("/whatson.jsp");}%>
