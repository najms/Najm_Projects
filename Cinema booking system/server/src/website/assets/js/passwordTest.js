var passwordValid = false;
var emailValid = false;

function checkPass()
{
    //Store the passwords as variables
    var pwd1 = document.getElementById('password');
    var pwd2 = document.getElementById('confirmpassword');
    var message = document.getElementById('submitMessage');

    //Set the colors
    var matching = "#99e500";
    var notMatching = "#e52800";
    //test if they are equal
    if(pwd2.value!=""){
    if(pwd1.value == pwd2.value){
        // text.style.backgroundColor = matching;
        // text1.style.backgroundColor = matching;
        message.innerHTML = "";
        passwordValid = true;
    }else{
        // text.style.backgroundColor = notMatching;
        // text1.style.backgroundColor = notMatching;
        message.style.color = notMatching;
        message.innerHTML = "Passwords Do Not Match!";
        passwordValid = false;
    }
  }
  checkButton();
}

function validateEmail()
{
  var email = document.getElementById('email');
  var message = document.getElementById('submitEmailMessage');
  if (/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(email.value)) {
    message.innerHTML = "Email Valid";
    emailValid = true;
  } else {
    message.innerHTML = "Email Invalid";
    emailValid = false;
  }
  checkButton();
}

function checkButton() {
  if (passwordValid && emailValid) {
    document.getElementById("createButton").disabled = false;
  } else {
    document.getElementById("createButton").disabled = true;
  }

}
