<!doctype html>
<%@ page import="server.Main" %>
<%
if (Main.connection.check(request, response, false)) {
%>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />

  <title>Information</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
  <link href="https://fonts.googleapis.com/css?family=Voltaire|Roboto|Overpass" rel="stylesheet">
  <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/muv.css" rel="stylesheet"/>




</head>


<body class="index-page">

  <nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a href="index.jsp"class="logo-container">
          <img src="images/logo.png" class="logo-container" id="logo-container">
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>


      <div class="collapse navbar-collapse site-navigation-row" id="navigation">

        <ul class="nav navbar-nav navbar-right">
          <li>
              <a href="whatson.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">WHAT'S ON</h2>
            </a>
          </li>
          <li>
            <a href="offers.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">OFFERS</h2>
            </a>
          </li>

          <li>
            <a href="vip.jsp" class=" link is-notactive mdl-navigation__link">
              <h2 class="nav-title">VIP</h2>
            </a>
          </li>
          <li>
            <a href="info.jsp" class="link active mdl-navigation__link">
              <h2 class="nav-title">INFORMATION</h2>
            </a>
          </li>
          <li class="nav-item">
              <a href="profile.jsp" class="nav-link profile">
                  <i class="material-icons profile-container">account_circle</i>
                  <h6>Profile</h6>
              </a>
          </li>
          <% if (Main.login.checkLogin(request)) { %>
          <li class="nav-item">
              <a href="signOut.jsp" class="nav-link profile">
                  <i class="material-icons join-container">event_seat</i>
                  <h6>Log Out</h6>
              </a>
          </li>
          <%} else { %>
          <li class="nav-item">
              <a href="Login.jsp" class="nav-link profile">
                  <i class="material-icons join-container">event_seat</i>
                  <h6>sign in</h6>
              </a>
          </li>
          <% } %>
          <li>
            <form action="search.jsp" method="get">
              <input type="text" class="input-lg input search" name="search" placeholder="Search...">
            </form>
            </li>
        </ul>
      </div>
    </div>
  </nav>



    <a class="btn pulse cd-top" href="#0" title="Back to top" id="myBtn">
    <i class="material-icons">arrow_upward</i></a>
    <div class="wrapper">


        <div class="header wrapper">

          <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item header-filter">
                  <img src="images/movingsound1.jpg" alt="mvsound" style="width:100%;">
                  <div class="carousel-caption d-none d-md-block">
                  </div>
                </div>

                <div class="item active">
                  <img src="images/dolbyAtmos.jpg" alt="Dolby" style="width:100%;">
                  <div class="carousel-caption d-none d-md-block">
                  </div>
                </div>
              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>

              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
          </div>


      <div class="main">
        <div class="container">
          <div class="section text-center section-index">
            <div class="row">
              <div class="col-md-6 col-md-offset-2">

              </div>
            </div>
            <hr align="left">

            <div class="animation-element bounce-up cf">
                <div class="subject development">
                  <h2 class="title">Quick Info</h2>

                      <div class="row">
                        <div class="col-md-6">
                          <h2>Contact Us</h2>
                          <div class="">
                            <hr>
                          <h3>Telephone:</h3><p><a href="tel:0121 211 4620"></a>
                                <span class="footer-title">0121 211 4620</span></p>
                          <h3>Email:</h3><p>Muv@cinema.com</p>
                          <h3>address:</h3><p>The Light 22<br>The Headrow Leeds<br> LS1 8TL</p>
                          <hr>
                            <h3>Book online or in store!</h3>
                        </div>
                        </div>

                        <div class="col-md-6">
                          <h2>FAQ</h2>
                          <hr>
                          <div class="">
                          <h3>What is VIP?</h3>
                          <p>Vip is an Upgrade in seat quality and seat positioning<br> This can be a one time purchase or a pay monthly deal.</p>
                          <h3>Does Muv provide discounts?</h3>
                          <p>MUV has a wide variety of offers, visit <a href="offers.jsp">HERE</a> for details</p>
                          <h3>What is MUV's opening times?</h3>
                          <p>10am-12am Mon-Fri<br> 10am-2am Sat&Sun</p>
                        </div><hr>
                        <a type="button" href="info.jsp" class="btn buttonG">I have a different query</a>
                        </div>
                      </div>
                    </div>



<hr align="left">
  <div class="subject development">
<h2 class="title">More About Us</h2>
<div class="row">
  <div class="col-md-6">
    <h2>About MUV</h2><hr>
    <p>MUV, Although small, is seen to become one of Europe's Leading cinema operators.
      Our most advanced cinema is located within leeds, with next nevel sound and vison technology.
      Never miss a moment of the big screen entertainment with our stepped, stadium-style seating.
      Whether you like to spread out in the middle or cosy up in the back, our seats mean that you'll get a perfect,
      unobstructed view of the screen, every time.</p>
      <p>We believe the best entertainment deserves the best quality picture. That's why every one of our screens is
      equipped with Sony 4K digital projectors, giving you a viewing experience that's so super sharp you'll feel like it's
      you fighting the bad guys. With four times the resolution of HD home TVs and more than four times detail than
      regular cinema projectors, it means brighter colours, more contrast and a picture that's always crystal clear, no matter
      where you sit.</p>

  </div>

  <div class="col-md-6">

    <h2>MUV's location</h2><hr>
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2356.405970405326!2d-1.5478390482105526!3d53.8000594799789!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48795c1c7c559bcf%3A0x481476e5cc8c8d89!2sThe+Light!5e0!3m2!1sen!2suk!4v1521135150622" width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>
<br><p></p></div>



  </div>


  <footer class="panel-footer" role="contentinfo">


      <section class="container">
          <h2 class="title2">
              Customer service

              <a href="tel:0121 211 4620"></a>
                    <span class="footer-title">0121 211 4620</span>
          </h2>
          <p>Call will be charged at your local rate.</p>

          <div class="title2">SOCIAL</div>
          <ul>
            <li><a href="#" class="fa fa-facebook"></a></li>
            <li><a href="#" class="fa fa-twitter"></a></li>
            <li><a href="#" class="fa fa-youtube"></a></li>
            <li><a href="#" class="fa fa-instagram"></a></li>
          </ul>
          <div class="title2">GET IN TOUCH</div>
          <ul>
            <li><a href="info.jsp" class="footer-title">FAQ</a></li>
            <li><a href="info.jsp" class="footer-title">Contact us</a></li>
            <li><a href="info.jsp" class="footer-title">Feedback</a></li>
            </ul>
            <div class="title2">LEGAL</div>
            <ul>
              <li><a href="info.jsp" class="footer-title">About us</a></li>
              <li><a href="info.jsp" class="footer-title">Terms & Conditions</a></li>
              <li><a href="info.jsp" class="footer-title">Privacy</a></li>
              <li><a href="info.jsp" class="footer-title">Accessability</a></li>
              </ul>
                      </section>

                      <div>
                      <section href="index.jsp"class="container ">
                        <img src="images/logo.png" class="logo-footer container">
                      </section>
                    </div>



                      <hr class="side">




      <div>
          <section class="container ">
              <p> &#169; MUV. All rights reserved. </p>
          </section>
      </div>
    </footer>







        <!--   Core JS Files   -->
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <!-- <script src="assets/js/bootstrap.min.js" type="text/javascript"></script> -->
        <script src="assets/js/counter.min.js" type="text/javascript"></script>
        <script src="assets/js/headingsindex.min.js" type="text/javascript"></script>
        <script src="assets/js/material.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="js/scrollreveal.min.js"></script>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="assets/js/jquery.waypoints.waypoints.min.js" type="text/javascript"></script>

        <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
        <script src="assets/js/material-kit.js" type="text/javascript"></script>
        <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

        <% } else {
        response.sendRedirect("/index.jsp");
        } %>
