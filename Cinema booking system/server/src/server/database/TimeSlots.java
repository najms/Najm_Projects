package server.database;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * This holds the information about what time is each movie showing each movie
 * having an id
 */
public class TimeSlots {

  private int timeId;
  private Timestamp startTime;
  private int screen;
  private int timeslot_movieId;

  /**
   * getter for the timeId
   * @return timeId
   */
  public int getTimeId() {

    return timeId;
  }

  /**
   * getter for the startTime
   * @return startTime
   */
  public Calendar getStartTime() {
    Calendar start = Calendar.getInstance();
    start.setTimeInMillis(startTime.getTime());
    return start;
  }

  /**
   * getter for the endTime
   * @return start
   */
  public Calendar getEndTime() {
    Calendar start = getStartTime();
    start.add(Calendar.HOUR_OF_DAY, 3);
    return start;
  }

  /**
   * getter for the screenNumber
   * @return screen
   */
  public int getScreen() {

    return screen;
  }

  /**
   * getter for the timeslot_movieId
   * @return timeslot_movieId
   */
  public int getTimeslot_movieId() {

    return timeslot_movieId;
  }

}
