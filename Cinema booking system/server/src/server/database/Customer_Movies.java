package server.database;

/**
 * This holds all the data when a customer has bought a ticket this can be used
 * to print out the ticket details
 */
public class Customer_Movies {

  private int customerMoviesId;
  private int customerMovies_movieId;
  private int customerMovies_customerId;
  private int customerMovies_ticketId;
  private String customerMovies_staffId;

  /**
   * getter to get the customerMovieID
   * @return customerMoviesId
   */
  public int getCustomerMoviesId() {

    return customerMoviesId;
  }

  /**
   * getter to get the customerMovieID_movieId which links the tables together
   * @return customerMovies_movieId
   */
  public int getGetCustomerMovies_movieId() {

    return customerMovies_movieId;
  }

  /**
   * getter to get the customerMovies_customerId
   * @return customerMovies_customerId
   */
  public int getGetCustomerMovies_customerId() {

    return customerMovies_customerId;
  }

  /**
   * getter to get the CustomerMovies_ticketId
   * @return CustomerMovies_ticketId
   */
  public int getGetCustomerMovies_ticketId() {

    return customerMovies_ticketId;
  }

  /**
   * getter to get the GetCustomerMovies_staffId
   * @return GetCustomerMovies_staffId
   */
  public String getGetCustomerMovies_staffId() {

    return customerMovies_staffId;
  }
}
