package server.database;

import server.Main;

/**
 * This class deals with logged in users , stores their information and allows
 * them to sign in which would allow them to recieve VIP discounts
 */

public class Customers {

  private int customersId;
  private String name;
  private String surname;
  private String cardNumber;
  private boolean guestFlag;
  private String email;

  private String username;
  private byte[] password;
  private byte[] salt;

  /**
   * getter to get the customerId
   * @return customersId
   */
  public int getCustomersId() {

    return customersId;
  }

  /**
   * getter to get the customer name
   * @return name
   */
  public String getName() {

    return name;
  }

  /**
   * getter to get the customer surname
   * @return surname
   */
  public String getSurname() {

    return surname;
  }

  /**
   * getter to get the customer cardNumber
   * @return cardNumber
   */
  public String getCardNumber() {

    return Main.database.anonymizeCard(cardNumber);
  }

  /**
   * getter to get the customer cardNumber
   * @return cardNumber
   */
  public String getRealCardNumber() {

    return cardNumber;
  }

  /**
   * getter to the get the customer guestFlag
   * @return guestFlag
   */
  public boolean getGuestFlag() {

    return guestFlag;
  }

  /**
   * getter to get the customer Email
   * @return email
   */
  public String getEmail() {

    return email;
  }

  /**
   * getter to get the customer username
   * @return username
   */
  public String getUsername() {

    return username;
  }

  /**
   * getter to get the customer password
   * @return password
   */
  public byte[] getPassword() {

    return password;
  }

  /**
   * getter to get the customer salt
   * @return salt
   */
  public byte[] getSalt() {

    return salt;
  }

}
