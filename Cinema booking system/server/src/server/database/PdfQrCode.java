package server.database;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.FontSelector;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.VerticalPositionMark;

import server.Main;

/**
 * This class produces a qrCode and PDF for the receipts
 */
public class PdfQrCode {
  private static String directory = "./tickets/";
  private static DecimalFormat decFormat = new DecimalFormat("#.00");

  private static void checkDirectory() {
    File folder = new File(directory);
    if (!folder.exists()) {
      folder.mkdir();
    }
  }

  /**
   * This generates a qrCode with the text inside the qrcode with specific size
   * , fileType and passes the file of where to save the QrCode
   *
   * @param text
   * @param size
   * @param fileType
   * @param file
   */
  public static void generateQr(String text, int size, String fileType,
                                File file) {
    Hashtable<EncodeHintType,
              ErrorCorrectionLevel> hashtable =
                                              new Hashtable<EncodeHintType,
                                                            ErrorCorrectionLevel>();
    // put the correction level for them matrix the level of detail
    hashtable.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
    QRCodeWriter qrwriter = new QRCodeWriter();
    // matrix of bits insert image
    try {
      BitMatrix bitMatrix = qrwriter.encode(text, BarcodeFormat.QR_CODE, size,
                                            size, hashtable);
      // put everything in an image file
      BufferedImage image = new BufferedImage(size, size,
                                              BufferedImage.TYPE_INT_RGB);
      image.createGraphics();
      // creating the square around white background and black outside
      Graphics2D graphics = (Graphics2D) image.getGraphics();
      graphics.setColor(Color.white);
      graphics.fillRect(0, 0, size, size);
      graphics.setColor(Color.BLACK);
      // loop through and fill each pixel
      for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
          if (bitMatrix.get(i, j)) {
            graphics.fillRect(i, j, 1, 1);
          }
        }
      }
      // writes the image to the file with the type
      ImageIO.write(image, fileType, file);

    } catch (WriterException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * This produces a reciept for the user online taking their
   * name,cardnumber,totalcost,list of tickets, vip flags and seat Numbers. a
   * reciept for the total cost is produced and a qr code for each ticket which
   * will be used instore- containing information like movie,showing,screen,seat
   * if vip and time.
   *
   * @param name
   * @param email
   * @param cardNumber
   * @param totalCost
   * @param ticket
   * @param vipflag
   * @param seatNumbers
   * @return
   */
  public static String recieptOnlineUser(String name, String email,
                                         String cardNumber, double totalCost,
                                         List<Tickets> ticket, boolean vipflag,
                                         String seatNumbers) {
    checkDirectory();
    String filepath = directory + "qrCodeTicket" + ticket.get(0).getTicketId()
                      + ".pdf";
    String muvLogo = "./src/images/bannertop.jpg";
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm");
    List<String> seatList = null;
    if (vipflag) {
      seatList = Arrays.asList(seatNumbers.split(","));
    }

    // create a document
    Document document = new Document(PageSize.A6);
    String cardnumberHidden = Main.database.anonymizeCard(cardNumber);
    try {
      // create an instance for it to be generated
      PdfWriter writer = PdfWriter.getInstance(document,
                                               new FileOutputStream(filepath));
      document.open();
      PdfContentByte pdfcontentbyte = writer.getDirectContent();

      // add logo
      Image logo = Image.getInstance(muvLogo);
      logo.scaleAbsolute(200f, 100f);
      logo.setAlignment(Image.MIDDLE);
      document.add(logo);

      // add font and size
      FontSelector selector = new FontSelector();
      Font f1 = FontFactory.getFont(FontFactory.COURIER_BOLD, 10);
      f1.setColor(Color.black);
      selector.addFont(f1);
      Phrase nameph = selector.process("\t\t\tHello " + name);
      Phrase cardstatementph = selector.process("\t\t\tCard Number :");
      Phrase cardnumberph = selector.process(cardnumberHidden);
      Phrase costph = selector.process("Amount : \u00A3"
                                       + decFormat.format(totalCost));
      Phrase footer = selector.process("Thank You For your service");

      // add text
      document.add(new Paragraph(""));
      document.add(new Paragraph(""));
      document.add(new Paragraph(nameph));
      document.add(new Paragraph(""));
      // add card details
      Chunk glue = new Chunk(new VerticalPositionMark());
      Paragraph p = new Paragraph(cardstatementph);
      p.add(new Chunk(glue));
      p.add(cardnumberph);
      document.add(p);
      // add amount paid
      document.add(new Paragraph(""));
      Chunk glue2 = new Chunk(new VerticalPositionMark());
      Paragraph p2 = new Paragraph();
      p2.add(new Chunk(glue2));
      p2.add(costph);
      document.add(p2);

      // creating a QrCode for each ticket
      for (int i = 0; i < ticket.size(); i++) {
        document.newPage();

        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector.process("MUV : MovieTicket"),
                                   (document.right() - document.left()) / 2
                                                                          + document.leftMargin(),
                                   document.top() + 15, 0);
        Movies movie = Main.database.getMovieFromTicket(ticket.get(i)
                                                              .getTicketId());
        String movieName = movie.getName();
        String ageRating = movie.getCertificate();
        TimeSlots slot = Main.database.getSlotFromTicket(ticket.get(i)
                                                               .getTicketId());
        int screen = slot.getScreen();
        Calendar start = slot.getStartTime();
        String showingTime = sdf.format(start.getTime());

        File file = new File(directory + "qrCodeTicketOnline.png");
        String fileType = "png";
        String text;
        if (vipflag) {
          text = "TicketId :" + ticket.get(i).getTicketId() + "\n" + movieName
                 + "\n" + ageRating + "\n Screen: " + screen + "\n"
                 + showingTime + ticket.get(i).getCustomerType() + "\n"
                 + seatList.get(i);
        } else {
          text = "TicketId :" + ticket.get(i).getTicketId() + "\n" + movieName
                 + "\n" + ageRating + "\n Screen: " + screen + "\n"
                 + showingTime + ticket.get(i).getCustomerType();
        }

        generateQr(text, 200, fileType, file);

        // add qrcode
        Image img = Image.getInstance(directory + "qrCodeTicketOnline.png");
        img.scaleAbsolute(150f, 150f);
        img.setAlignment(Image.MIDDLE);
        document.add(img);

        FontSelector selector3 = new FontSelector();
        Font f3 = FontFactory.getFont(FontFactory.COURIER_OBLIQUE, 12);
        f3.setColor(Color.black);
        selector3.addFont(f3);

        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process(movieName),
                                   (document.right() - document.left()) / 2
                                                                 + document.leftMargin(),
                                   document.top() - 160, 0);

        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process("Screen : " + screen),
                                   (document.right() - document.left()) / 2
                                                                            + document.leftMargin(),
                                   document.top() - 170, 0);

        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process(showingTime),
                                   (document.right() - document.left()) / 2
                                                                   + document.leftMargin(),
                                   document.top() - 180, 0);

        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process(ticket.get(i)
                                                           .getCustomerType()),
                                   (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                   document.top() - 190, 0);
        if (vipflag) {

          ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                     selector.process("VIP Seat :"
                                                      + seatList.get(i)),
                                     (document.right() - document.left()) / 2
                                                                          + document.leftMargin(),
                                     document.top() - 200, 0);

        } else {
          ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                     selector.process("Standard Seat"),
                                     (document.right() - document.left()) / 2
                                                                        + document.leftMargin(),
                                     document.top() - 200, 0);

        }

        System.out.println(149);
        // footer
        ColumnText.showTextAligned(writer.getDirectContent(),
                                   Element.ALIGN_LEFT, footer, 100, 30, 0);
      }

      document.close();

      sendEmail(email, ticket.get(0));

    } catch (DocumentException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return filepath.substring(directory.length());
  }

  /**
   * This produces a reciept for the user at the till using cash taking their
   * name,totalcost,list of tickets, each individual ticket details (
   * customertype and price) vip flags and seat Numbers. a reciept for the total
   * cost is produced and a qr code for each ticket which will be used outside
   * the screen- containing information like movie,showing,screen,seat if vip
   * and time.
   *
   * @param moneyGiven
   * @param totalCost
   * @param change
   * @param children
   * @param priceallChildren
   * @param adults
   * @param priceallAdults
   * @param seniors
   * @param priceallSeniors
   * @param seatNumbers
   * @param ticket
   * @param vipSeatFlag
   * @return
   */
  public static String
         recieptTillCash(double moneyGiven, double totalCost, double change,
                         int children, double priceallChildren, int adults,
                         double priceallAdults, int seniors,
                         double priceallSeniors, String seatNumbers,
                         List<Tickets> ticket, boolean vipSeatFlag) {
    checkDirectory();
    String filepath = directory + "tillCashReciept"
                      + ticket.get(0).getTicketId() + ".pdf";
    String qrCode = directory + "qrCodeToWebsite.png";

    // create a document
    Document document = new Document(PageSize.A8);
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm");
    String date1 = sdf.format(cal.getTime());

    // create a qr code to link back to website
    File file = new File(qrCode);
    String fileType = "png";
    String website = "www.muv.co.uk";
    generateQr(website, 200, fileType, file);

    List<String> seatList = null;
    if (vipSeatFlag) {
      seatList = Arrays.asList(seatNumbers.split(","));
    }

    try {
      // create an instance for it to be generated
      PdfWriter writer = PdfWriter.getInstance(document,
                                               new FileOutputStream(filepath));
      document.open();
      PdfContentByte pdfcontentbyte = writer.getDirectContent();
      // font
      FontSelector selector = new FontSelector();
      FontSelector selector1 = new FontSelector();
      Font f1 = FontFactory.getFont(FontFactory.COURIER, 5);
      f1.setColor(Color.black);
      Font f2 = FontFactory.getFont(FontFactory.COURIER_BOLD, 5);
      f2.setColor(Color.black);
      selector.addFont(f1);
      selector1.addFont(f2);

      Phrase header = selector.process("Muv");
      Phrase caption = selector.process(" Those MUV moments matter");
      Phrase date = selector.process(date1);
      Phrase children1 = selector.process("Children " + children);
      Phrase adults1 = selector.process("Adults " + adults);
      Phrase seniors1 = selector.process("Seniors " + seniors);
      Phrase childrenprice =
                           selector.process("£ "
                                            + decFormat.format(priceallChildren));
      Phrase adultprice = selector.process("£ "
                                           + decFormat.format(priceallAdults));
      Phrase seniorsprice =
                          selector.process("£ "
                                           + decFormat.format(priceallSeniors));
      Phrase moneygiven1 = selector.process("CASH   £ "
                                            + decFormat.format(moneyGiven));
      Phrase totalcost1 = selector.process("TOTAL  £ "
                                           + decFormat.format(totalCost));
      Phrase change1 =
                     selector.process("CHANGE £ " + decFormat.format(change));

      Phrase seperator = selector.process(" *******************");
      Phrase advert1 =
                     selector1.process("BECOME A MEMBER TODAY FOR EXCLUSIVE SAVINGS");
      Phrase advert2 =
                     selector.process("Scan the QRCODE below and visit our website");
      Phrase advert3 =
                     selector.process("to sign up and recieve numerous discounts daily ");
      Phrase advert4 = selector.process("AND");
      Phrase advert5 = selector1.process("To Win Prizes");

      // headings
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, header,
                                 (document.right() - document.left()) / 2
                                                                               + document.leftMargin(),
                                 document.top() + 20, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, caption,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() + 15, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, date,
                                 (document.right() - document.left()) / 2
                                                                             + document.leftMargin(),
                                 document.top() + 10, 0);

      // number of tickets on the left
      if (seniors > 0) {
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_RIGHT,
                                   seniors1,
                                   (document.right() - document.left()) / 5
                                             + document.leftMargin(),
                                   document.top() - 0, 0);
        // add prices on the right
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT,
                                   seniorsprice, 100, document.top() - 0, 0);
      }
      if (adults > 0) {
        // add type of purchase on right
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_RIGHT, adults1,
                                   (document.right() - document.left()) / 5
                                                                                 + document.leftMargin(),
                                   document.top() - 10, 0);
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT,
                                   adultprice, 100, document.top() - 10, 0);
      }
      if (children > 0) {
        // add type purchase on right
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_RIGHT,
                                   children1,
                                   (document.right() - document.left()) / 5
                                              + document.leftMargin(),
                                   document.top() - 20, 0);

        // add prive on right
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT,
                                   childrenprice, 100, document.top() - 20, 0);
      }
      // total
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT, totalcost1,
                                 100, document.top() - 40, 0);
      // cash
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT,
                                 moneygiven1, 100, document.top() - 50, 0);
      // change given
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT, change1,
                                 100, document.top() - 60, 0);

      // seperator
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT, seperator,
                                 (document.right() - document.left()) / 10
                                                                                + document.leftMargin(),
                                 document.top() - 75, 0);

      // advertisement
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert1,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 85, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert2,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 95, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert3,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 100, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert4,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 105, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert5,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 110, 0);

      // add qrcode
      Image img = Image.getInstance(qrCode);
      img.scaleAbsolute(50, 50);
      img.setAbsolutePosition(50, 15);
      document.add(img);

      // seperator
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT, seperator,
                                 (document.right() - document.left()) / 10
                                                                                + document.leftMargin(),
                                 document.top() - 160, 0);

      // next page
      int pagesRequired = children + adults + seniors;
      for (int i = 0; i < pagesRequired; i++) {
        FontSelector selector3 = new FontSelector();
        Font f3 = FontFactory.getFont(FontFactory.COURIER_OBLIQUE, 12);
        f3.setColor(Color.black);
        selector3.addFont(f3);

        document.newPage();

        Movies movie = Main.database.getMovieFromTicket(ticket.get(i)
                                                              .getTicketId());
        String movieName = movie.getName();
        String ageRating = movie.getCertificate();
        TimeSlots slot = Main.database.getSlotFromTicket(ticket.get(i)
                                                               .getTicketId());
        int screen = slot.getScreen();
        Calendar start = slot.getStartTime();
        String showingTime = sdf.format(start.getTime());

        // MovieTitle
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process(movieName),
                                   (document.right() - document.left()) / 2
                                                                 + document.leftMargin(),
                                   document.top() - 10, 0);
        // MovieAgeRating
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector1.process(ageRating),
                                   (document.right() - document.left()) / 2
                                                                 + document.leftMargin(),
                                   document.top() - 15, 0);
        // MovieScreen
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process("Screen  " + screen),
                                   (document.right() - document.left()) / 2
                                                                           + document.leftMargin(),
                                   document.top() - 25, 0);

        if (vipSeatFlag) {
          // Seat Number
          ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                     selector3.process("Seat Number "
                                                       + seatList.get(i)),
                                     (document.right() - document.left()) / 2
                                                                           + document.leftMargin(),
                                     document.top() - 35, 0);
        }
        // Customer Type time
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process("Customer : "
                                                     + ticket.get(i)
                                                             .getCustomerType()),
                                   (document.right() - document.left()) / 2
                                                                                  + document.leftMargin(),
                                   document.top() - 40, 0);

        // showing time
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process("Showing " + showingTime),
                                   (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                   document.top() - 45, 0);

        // generate a qr code for each ticket
        String text;
        if (vipSeatFlag) {
          text = "TicketId :" + ticket.get(i).getTicketId() + "\n" + "Screen  "
                 + screen + "\n" + "Seat Number " + seatList.get(i) + "\n"
                 + "Showing " + showingTime + "\n"
                 + ticket.get(i).getCustomerType();
        } else {
          text = "TicketId :" + ticket.get(i).getTicketId() + "\n" + "Screen  "
                 + screen + "\n" + "Showing " + showingTime + "\n"
                 + ticket.get(i).getCustomerType();
        }

        File file1 = new File(directory + "ticketQrCode.png");
        generateQr(text, 200, "png", file1);

        // add qrcode
        Image ticketimg = Image.getInstance(directory + "ticketQrCode.png");
        ticketimg.scaleAbsolute(100, 100);
        ticketimg.setAbsolutePosition(25, 25);
        document.add(ticketimg);

        // Seat Number
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector.process("Thankyou , Enjoy your movie"),
                                   (document.right() - document.left()) / 2
                                                                                    + document.leftMargin(),
                                   document.top() - 150, 0);
      }

      document.close();
    } catch (DocumentException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return filepath.substring(directory.length(), filepath.length());
  }

  /**
   * *This produces a reciept for the user at the till using their card taking
   * totalcost,list of tickets, each individual ticket details ( customertype
   * and price) vip flags and seat Numbers. a reciept for the total cost is
   * produced and a qr code for each ticket which will be used outside the
   * screen- containing information like movie,showing,screen,seat if vip and
   * time.
   *
   * @param totalCost
   * @param cardNumber
   * @param children
   * @param priceallChildren
   * @param adults
   * @param priceallAdults
   * @param seniors
   * @param priceallSeniors
   * @param seatNumbers
   * @param ticket
   * @param vipSeatFlag
   * @return
   */
  public static String recieptTillCard(double totalCost, String cardNumber,
                                       int children, double priceallChildren,
                                       int adults, double priceallAdults,
                                       int seniors, double priceallSeniors,
                                       String seatNumbers, List<Tickets> ticket,
                                       boolean vipSeatFlag) {
    checkDirectory();
    String filepath = directory + "tillCashReciept"
                      + ticket.get(0).getTicketId() + ".pdf";
    String qrCode = directory + "qrCodeToWebsite.png";
    // create WebsiteQrCode

    // create a document
    Document document = new Document(PageSize.A8);
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm");

    // initialise the list of seatList
    List<String> seatList = null;
    if (vipSeatFlag) {
      seatList = Arrays.asList(seatNumbers.split(","));
    }

    String date1 = sdf.format(cal.getTime());
    String cardnumberHidden = Main.database.anonymizeCard(cardNumber);

    try {
      // create an instance for it to be generated
      PdfWriter writer = PdfWriter.getInstance(document,
                                               new FileOutputStream(filepath));
      document.open();
      PdfContentByte pdfcontentbyte = writer.getDirectContent();
      // font
      FontSelector selector = new FontSelector();
      FontSelector selector1 = new FontSelector();
      Font f1 = FontFactory.getFont(FontFactory.COURIER, 5);
      f1.setColor(Color.black);
      Font f2 = FontFactory.getFont(FontFactory.COURIER_BOLD, 5);
      f2.setColor(Color.black);
      selector.addFont(f1);
      selector1.addFont(f2);

      Phrase header = selector.process("Muv");
      Phrase caption = selector.process(" Those MUV moments matter");
      Phrase date = selector.process(date1);
      Phrase children1 = selector.process("Children " + children);
      Phrase adults1 = selector.process("Adults " + adults);
      Phrase seniors1 = selector.process("Seniors " + seniors);
      Phrase childrenprice =
                           selector.process("£ "
                                            + decFormat.format(priceallChildren));
      Phrase adultprice = selector.process("£ "
                                           + decFormat.format(priceallAdults));
      Phrase seniorsprice =
                          selector.process("£ "
                                           + decFormat.format(priceallSeniors));
      Phrase cardNumber1 = selector.process("CARD : " + cardnumberHidden);
      Phrase totalcost1 = selector.process("TOTAL  £ "
                                           + decFormat.format(totalCost));

      Phrase seperator = selector.process(" *******************");
      Phrase advert1 =
                     selector1.process("BECOME A MEMBER TODAY FOR EXCLUSIVE SAVINGS");
      Phrase advert2 =
                     selector.process("Scan the QRCODE below and visit our website");
      Phrase advert3 =
                     selector.process("to sign up and recieve numerous discounts daily ");
      Phrase advert4 = selector.process("AND");
      Phrase advert5 = selector1.process("To Win Prizes");

      // headings
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, header,
                                 (document.right() - document.left()) / 2
                                                                               + document.leftMargin(),
                                 document.top() + 20, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, caption,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() + 15, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, date,
                                 (document.right() - document.left()) / 2
                                                                             + document.leftMargin(),
                                 document.top() + 10, 0);

      // number of tickets on the left
      if (seniors > 0) {
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_RIGHT,
                                   seniors1,
                                   (document.right() - document.left()) / 5
                                             + document.leftMargin(),
                                   document.top() - 0, 0);
        // add prices on the right
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT,
                                   seniorsprice, 100, document.top() - 0, 0);
      }
      if (adults > 0) {
        // add type of purchase on right
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_RIGHT, adults1,
                                   (document.right() - document.left()) / 5
                                                                                 + document.leftMargin(),
                                   document.top() - 10, 0);
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT,
                                   adultprice, 100, document.top() - 10, 0);
      }
      if (children > 0) {
        // add type purchase on right
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_RIGHT,
                                   children1,
                                   (document.right() - document.left()) / 5
                                              + document.leftMargin(),
                                   document.top() - 20, 0);

        // add prive on right
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT,
                                   childrenprice, 100, document.top() - 20, 0);
      }
      // total
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT, totalcost1,
                                 100, document.top() - 40, 0);
      // card details
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT,
                                 cardNumber1, 50, document.top() - 60, 0);

      // seperator
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT, seperator,
                                 (document.right() - document.left()) / 10
                                                                                + document.leftMargin(),
                                 document.top() - 75, 0);

      // advertisement
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert1,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 85, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert2,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 95, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert3,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 100, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert4,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 105, 0);
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER, advert5,
                                 (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                 document.top() - 110, 0);

      // add qrcode
      Image img = Image.getInstance(qrCode);
      img.scaleAbsolute(50, 50);
      img.setAbsolutePosition(50, 15);
      document.add(img);

      // seperator
      ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_LEFT, seperator,
                                 (document.right() - document.left()) / 10
                                                                                + document.leftMargin(),
                                 document.top() - 160, 0);

      // next page
      int pagesRequired = children + adults + seniors;
      for (int i = 0; i < pagesRequired; i++) {
        FontSelector selector3 = new FontSelector();
        Font f3 = FontFactory.getFont(FontFactory.COURIER_OBLIQUE, 12);
        f3.setColor(Color.black);
        selector3.addFont(f3);

        document.newPage();

        Movies movie = Main.database.getMovieFromTicket(ticket.get(i)
                                                              .getTicketId());
        String movieName = movie.getName();
        String ageRating = movie.getCertificate();
        TimeSlots slot = Main.database.getSlotFromTicket(ticket.get(i)
                                                               .getTicketId());
        int screen = slot.getScreen();
        Calendar start = slot.getStartTime();
        String showingTime = sdf.format(start.getTime());

        // MovieTitle
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process(movieName),
                                   (document.right() - document.left()) / 2
                                                                 + document.leftMargin(),
                                   document.top() - 10, 0);
        // MovieAgeRating
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector1.process(ageRating),
                                   (document.right() - document.left()) / 2
                                                                 + document.leftMargin(),
                                   document.top() - 15, 0);
        // MovieScreen
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process("Screen  " + screen),
                                   (document.right() - document.left()) / 2
                                                                           + document.leftMargin(),
                                   document.top() - 25, 0);

        if (vipSeatFlag) {
          // Seat Number
          ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                     selector3.process("Vip Seat : "
                                                       + seatList.get(i)),
                                     (document.right() - document.left()) / 2
                                                                           + document.leftMargin(),
                                     document.top() - 35, 0);
        } else {
          // Seat Number
          ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                     selector3.process("Standard Seat"),
                                     (document.right() - document.left()) / 2
                                                                         + document.leftMargin(),
                                     document.top() - 35, 0);

        }
        // Customer Type time
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process("Customer : "
                                                     + ticket.get(i)
                                                             .getCustomerType()),
                                   (document.right() - document.left()) / 2
                                                                                  + document.leftMargin(),
                                   document.top() - 40, 0);

        // showing time
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector3.process("Showing " + showingTime),
                                   (document.right() - document.left()) / 2
                                                                                + document.leftMargin(),
                                   document.top() - 45, 0);

        // generate a qr code for each ticket
        String text;
        if (vipSeatFlag) {
          text = "TicketId :" + ticket.get(i).getTicketId() + "\n" + "Screen  "
                 + screen + "\n" + "Seat Number " + seatList.get(i) + "\n"
                 + "Showing " + showingTime + "\n"
                 + ticket.get(i).getCustomerType();
        } else {
          text = "TicketId :" + ticket.get(i).getTicketId() + "\n" + "Screen  "
                 + screen + "\n" + "Showing " + showingTime + "\n"
                 + ticket.get(i).getCustomerType();
        }

        File file = new File(directory + "ticketQrCode.png");
        generateQr(text, 200, "png", file);

        // add qrcode
        Image ticketimg = Image.getInstance(directory + "ticketQrCode.png");
        ticketimg.scaleAbsolute(100, 100);
        ticketimg.setAbsolutePosition(25, 25);
        document.add(ticketimg);

        // Seat Number
        ColumnText.showTextAligned(pdfcontentbyte, Element.ALIGN_CENTER,
                                   selector.process("Thank You , Enjoy your movie"),
                                   (document.right() - document.left()) / 2
                                                                                     + document.leftMargin(),
                                   document.top() - 150, 0);

      }

      document.close();

    } catch (DocumentException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return filepath.substring(directory.length(), filepath.length());
  }

  /**
   * This sends an email to the customer containing the PDF
   *
   * @param useremail
   * @param ticket
   */
  public static void sendEmail(String useremail, Tickets ticket) {

    String from = "merlinwizardtalib@gmail.com";
    // Get system properties
    Properties properties = System.getProperties();
    // Setup mail server
    properties.setProperty("mail.smtp.host", "localhost");
    // Get the default Session object.
    Session session = Session.getDefaultInstance(properties);
    // Create a default MimeMessage object.
    MimeMessage message = new MimeMessage(session);

    try {
      message.setFrom(new InternetAddress(from));
      message.addRecipient(Message.RecipientType.TO,
                           new InternetAddress(useremail));
      // subject of message
      message.setSubject("MUV cinema");
      // Create the message part
      BodyPart messageBodyPart = new MimeBodyPart();
      messageBodyPart.setText("Thankyou for your purchase please provide The Attached reciept at the checking stand");
      // Create a multipar message
      Multipart multipart = new MimeMultipart();
      // Set text message part
      multipart.addBodyPart(messageBodyPart);
      // send the attachment
      messageBodyPart = new MimeBodyPart();
      String filename = "reciept.pdf";
      DataSource source = new FileDataSource(directory + "qrCodeTicket"
                                             + ticket.getTicketId() + ".pdf");
      messageBodyPart.setDataHandler(new DataHandler(source));
      messageBodyPart.setFileName(filename);
      multipart.addBodyPart(messageBodyPart);
      // Set content of message
      message.setContent(multipart);
      // Send message
      Transport.send(message);
    } catch (MessagingException e) {
      e.printStackTrace();
    }
  }
}
