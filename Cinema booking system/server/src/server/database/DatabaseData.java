package server.database;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.sql2o.Connection;
import org.sql2o.Sql2o;

import server.Main;

/**
 * class to insert all the data into the table when there is no data inside the
 * tables
 */
public class DatabaseData {

  private boolean checkingTimeSlots = false;
  /**
   * initialise the driver
   */
  public Sql2o db = new Sql2o("jdbc:derby:./cinema;create=true", "", "");

  /**
   * method to add all data to tables
   */
  public void demo() {
    moviesTable();
    staffTable();
  }

  /**
   * Clears all the tables
   */
  public void clearTables() {
    Connection connection = db.open();
    connection.createQuery("DELETE FROM Customer_Movies").executeUpdate();
    connection.createQuery("DELETE FROM TICKETS").executeUpdate();
    connection.createQuery("DELETE FROM SEATS").executeUpdate();
    connection.createQuery("DELETE FROM TIMESLOTS").executeUpdate();
    connection.createQuery("DELETE FROM Images").executeUpdate();
    connection.createQuery("DELETE FROM Movies").executeUpdate();
    connection.createQuery("DELETE FROM Staff").executeUpdate();
    connection.createQuery("DELETE FROM Customers").executeUpdate();
    connection.commit();
  }

  /**
   * deletes the data from the table and inserts it all again incase it is
   * missing
   */
  public void staffTable() {
    Main.login.createAccountForStaff("Asim", "asim", "asim");
    Main.login.createAccountForStaff("Najm", "najm", "najm");
    Main.login.createAccountForStaff("Ali", "ali", "ali");
    Main.login.createAccountForStaff("Sule", "sule", "sule");
    Main.login.createAccountForStaff("Jamil", "jamil", "jamil");
    Main.login.createAccountForStaff("Dan", "dan", "dan");
  }

  /**
   * This inserts all the movies that will be available within the website
   */
  private void moviesTable() {
    int movieId = -1;

    System.out.println("Adding movies to database. This may take a while.");

    Calendar releaseDate = Calendar.getInstance();
    Calendar mDuration = Calendar.getInstance();

    releaseDate.set(2018, Calendar.FEBRUARY, 16, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 14, 0);
    movieId = insertNewMovie("Black Panther",
                             "T'Challa, after the death of his father, the King of Wakanda, returns home to the isolated, technologically advanced African nation to succeed to the throne and take his rightful place as king.",
                             "PG-13", "Ryan Coogler",
                             "Chadwick Boseman\nMichael B. Jordan\nLupita Nyong'o",
                             releaseDate,
                             "https://www.youtube.com/watch?v=xjDjIWPwcPU",
                             mDuration, 7.7, false, true, false, true, false);
    linkImageToMovie(movieId, "blackpantherbannertop.jpg");
    linkImageToMovie(movieId, "blackpantherbehind.jpg");
    linkImageToMovie(movieId, "blackpantherdetailsbanner.jpg");
    linkImageToMovie(movieId, "blackpantherinterview.jpg");
    linkImageToMovie(movieId, "blackpantherposter.jpg");
    linkImageToMovie(movieId, "blackpanthertrailer.jpg");

    releaseDate.set(2018, Calendar.JANUARY, 26, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 21, 0);
    movieId = insertNewMovie("Maze Runner Death Cure",
                             "Young hero Thomas embarks on a mission to find a cure for a deadly disease known as the \"Flare\"",
                             "PG-13", "Wes Ball",
                             "Dylan O'Brien\nKi Hong Lee\nKaya Scodelario",
                             releaseDate,
                             "https://www.youtube.com/watch?v=4-BTxXm8KSg",
                             mDuration, 6.5, false, false, false, true, false);
    linkImageToMovie(movieId, "mazerunnerdetailsbanner.png");
    linkImageToMovie(movieId, "mazerunnerthedeathcurebannertop.jpg");
    linkImageToMovie(movieId, "mazerunnerthedeathcurebehind.jpg");
    linkImageToMovie(movieId, "mazerunnerthedeathcureinterview.jpg");
    linkImageToMovie(movieId, "mazerunnerthedeathcureposter.jpg");
    linkImageToMovie(movieId, "mazerunnerthedeathcuretrailer.jpg");

    releaseDate.set(2017, Calendar.DECEMBER, 15, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 32, 0);
    movieId = insertNewMovie("Star Wars The Last Jedi",
                             "Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares for battle with the First Order",
                             "PG-13", "Rian Johnson",
                             "Daisy Ridley\nJohn Boyega\nMark Hamill",
                             releaseDate,
                             "https://www.youtube.com/watch?v=Q0CbN8sfihY",
                             mDuration, 7.4, false, true, false, true, false);
    linkImageToMovie(movieId, "starwarsbannertop.jpg");
    linkImageToMovie(movieId, "starwarsbehind.jpg");
    linkImageToMovie(movieId, "starwarsdetailsbanner.jpg");
    linkImageToMovie(movieId, "starwarsinterview.jpg");
    linkImageToMovie(movieId, "starwarsposter.jpg");
    linkImageToMovie(movieId, "starwarstrailer.jpg");

    releaseDate.set(2018, Calendar.APRIL, 16, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 29, 0);
    movieId = insertNewMovie("Avengers Infinity War",
                             "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe",
                             "PG-13", "Anthony Russo, Joe Russo ",
                             "Karen Gillan\nJosh Brolin\nLetitia Wright",
                             releaseDate,
                             "https://www.youtube.com/watch?v=6ZfuNTqbHE8",
                             mDuration, 8.0, true, true, false, false, false);
    linkImageToMovie(movieId, "avengersbehind.jpg");
    linkImageToMovie(movieId, "avengersdetailsbanner.jpg");
    linkImageToMovie(movieId, "avengersinterview.jpg");
    linkImageToMovie(movieId, "avengersposter.jpg");
    linkImageToMovie(movieId, "avengerstrailer.jpg");

    releaseDate.set(2017, Calendar.DECEMBER, 20, 0, 0, 0);
    mDuration.set(0, 0, 0, 1, 45, 0);
    movieId = insertNewMovie("The Greatest Showman",
                             "Celebrates the birth of show business, and tells of a visionary who rose from nothing to create a spectacle that became a worldwide sensation. ",
                             "PG", "Michael Gracey",
                             "Hugh Jackman\nMichelle Williams\nZac Efron",
                             releaseDate,
                             "https://www.youtube.com/watch?v=AXCTMGYUg9A",
                             mDuration, 7.8, true, true, true, false, false);
    linkImageToMovie(movieId, "thegreatestshowmanbehind.jpg");
    linkImageToMovie(movieId, "thegreatestshowmandetailsbanner.jpg");
    linkImageToMovie(movieId, "thegreatestshowmaninterview.jpg");
    linkImageToMovie(movieId, "thegreatestshowmanposter.jpg");
    linkImageToMovie(movieId, "thegreatestshowmantrailer.jpg");

    releaseDate.set(2017, Calendar.DECEMBER, 22, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 5, 0);
    movieId = insertNewMovie("Darkest Hour",
                             "In 1940 the fate of Western Europe hangs on the newly-appointed British Prime Minister Winston Churchill, who must decide whether to negotiate with Adolf Hitler, or fight on knowing it will mean the end of the British Empire",
                             "PG-13", "Joe Wright",
                             "Gary Oldman\nLily James\nKristin Scott Thomas",
                             releaseDate,
                             "https://www.youtube.com/watch?v=LtJ60u7SUSw",
                             mDuration, 7.4, true, false, false, true, false);
    linkImageToMovie(movieId, "darkesthourbannertop.jpg");
    linkImageToMovie(movieId, "darkesthourbehind.jpg");
    linkImageToMovie(movieId, "darkesthourdetailsbanner.jpg");
    linkImageToMovie(movieId, "darkesthourinterview.jpg");
    linkImageToMovie(movieId, "darkesthourposter.jpg");
    linkImageToMovie(movieId, "darkesthourtrailer.jpg");

    releaseDate.set(2013, Calendar.APRIL, 26, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 12, 0);
    movieId = insertNewMovie("Aashiqui 2",
                             "Rahul loses his fans and fame due to alcoholism. But he then decides to turn a small time singer into a rising star",
                             "PG-13", "Mohit Suri",
                             "Aditya Roy Kapoor\nShraddha Kapoor\nShaad Randhawa",
                             releaseDate,
                             "https://www.youtube.com/watch?v=FyXXgpPqe6w",
                             mDuration, 7.0, true, false, false, false, false);
    linkImageToMovie(movieId, "aashiqui2behind.jpg");
    linkImageToMovie(movieId, "aashiqui2detailsbanner.jpg");
    linkImageToMovie(movieId, "aashiqui2interview.jpg");
    linkImageToMovie(movieId, "aashiqui2poster.jpg");
    linkImageToMovie(movieId, "aashiqui2trailer.jpg");

    releaseDate.set(1994, Calendar.OCTOBER, 14, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 22, 0);
    movieId = insertNewMovie("Shawshank Redemption",
                             "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency. ",
                             "R", "Frank Darabont",
                             "Tim Robbins\nMorgan Freeman\nBob Gunton",
                             releaseDate,
                             "https://www.youtube.com/watch?v=6hB3S9bIaco",
                             mDuration, 9.3, false, false, false, false, true);
    linkImageToMovie(movieId, "shawshankredemptionbehind.jpg");
    linkImageToMovie(movieId, "shawshankredemptiondetailsbanner.jpg");
    linkImageToMovie(movieId, "shawshankredemptioninterview.jpg");
    linkImageToMovie(movieId, "shawshankredemptionposter.jpg");
    linkImageToMovie(movieId, "shawshankredemptiontrailer.jpg");

    releaseDate.set(2018, Calendar.JANUARY, 12, 0, 0, 0);
    mDuration.set(0, 0, 0, 1, 43, 0);
    movieId = insertNewMovie("Paddington 2",
                             "A young Peruvian bear travels to London in search of a home. Finding himself lost and alone at Paddington Station, he meets the kindly Brown family, who offer him a temporary haven.",
                             "PG", "Paul King",
                             "Hugh Bonneville\nSally Hawkins\nJulie Walters",
                             releaseDate,
                             "https://www.youtube.com/watch?v=5B6A0UrYJsk",
                             mDuration, 8.0, false, false, true, false, false);
    linkImageToMovie(movieId, "paddington2behind.jpg");
    linkImageToMovie(movieId, "paddington2detailsbanner.jpg");
    linkImageToMovie(movieId, "paddington2interview.jpg");
    linkImageToMovie(movieId, "paddington2poster.jpg");
    linkImageToMovie(movieId, "paddington2trailer.jpg");

    releaseDate.set(2009, Calendar.DECEMBER, 18, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 42, 0);
    movieId = insertNewMovie("Avatar",
                             "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
                             "PG-13", "James Cameron",
                             "Sam Worthington\nZoe Saldana\nSigourney Weaver",
                             releaseDate,
                             "https://www.youtube.com/watch?v=5PSNL1qE6VY",
                             mDuration, 7.8, false, false, false, false, true);
    linkImageToMovie(movieId, "avatarbehind.jpg");
    linkImageToMovie(movieId, "avatardetailsbanner.jpg");
    linkImageToMovie(movieId, "avatarinterview.jpg");
    linkImageToMovie(movieId, "avatarposter.jpg");
    linkImageToMovie(movieId, "avatartrailer.jpg");

    releaseDate.set(1997, Calendar.DECEMBER, 19, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 54, 0);
    movieId = insertNewMovie("Titanic",
                             "A seventeen-year-old aristocrat falls in love with a kind but poor artist aboard the luxurious, ill-fated R.M.S. Titanic",
                             "PG-13", "James Cameron",
                             "Leonardo DiCaprio\nKate Winslet\nBilly Zane",
                             releaseDate,
                             "https://www.youtube.com/watch?v=zCy5WQ9S4c0",
                             mDuration, 7.8, true, false, false, false, true);
    linkImageToMovie(movieId, "titanic.jpg");
    linkImageToMovie(movieId, "titanicbehind.jpg");
    linkImageToMovie(movieId, "titanicdetailsbanner.jpg");
    linkImageToMovie(movieId, "titanicinterview.jpg");
    linkImageToMovie(movieId, "titanicposter.jpg");
    linkImageToMovie(movieId, "titanictrailer.jpg");

    releaseDate.set(2010, Calendar.JULY, 16, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 28, 0);
    movieId = insertNewMovie("Inception",
                             "A thief, who steals corporate secrets through the use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.",
                             "PG-13", "Christopher Nolan",
                             "Leonardo DiCaprio\nJoseph Gordon-Levitt\nEllen Page",
                             releaseDate,
                             "https://www.youtube.com/watch?v=YoHD9XEInc0",
                             mDuration, 8.8, false, false, false, false, true);
    linkImageToMovie(movieId, "inceptionbehind.jpg");
    linkImageToMovie(movieId, "inceptiondetailsbanner.jpg");
    linkImageToMovie(movieId, "inceptioninterview.jpg");
    linkImageToMovie(movieId, "inceptionposter.jpg");
    linkImageToMovie(movieId, "inceptiontrailer.jpg");

    releaseDate.set(1994, Calendar.JULY, 6, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 22, 0);
    movieId =
            insertNewMovie("Forrest Gump",
                           "The presidencies of Kennedy and Johnson, Vietnam, Watergate, and other history unfold through the perspective of an Alabama man with an IQ of 75.",
                           "PG-13", "Robert Zemeckis",
                           "Tom Hanks\nRobin Wright\nGary Sinise", releaseDate,
                           "https://www.youtube.com/watch?v=bLvqoHBptjg",
                           mDuration, 8.8, true, false, true, false, true);
    linkImageToMovie(movieId, "forrestgumpbehind.jpg");
    linkImageToMovie(movieId, "forrestgumpdetailsbanner.jpg");
    linkImageToMovie(movieId, "forrestgumpinterview.jpg");
    linkImageToMovie(movieId, "forrestgumpposter.jpg");
    linkImageToMovie(movieId, "forrestgumptrailer.jpg");

    releaseDate.set(2017, Calendar.FEBRUARY, 24, 0, 0, 0);
    mDuration.set(0, 0, 0, 1, 44, 0);
    movieId = insertNewMovie("Get Out",
                             "A young African-American visits his white girlfriend's parents for the weekend, where his simmering uneasiness about their reception of him eventually reaches a boiling point.",
                             "R", "Jordan Peele",
                             "Daniel Kaluuya\nAllison Williams\nBradley Whitford",
                             releaseDate,
                             "https://www.youtube.com/watch?v=DzfpyUB60YY",
                             mDuration, 7.7, false, false, false, false, false);
    linkImageToMovie(movieId, "getoutbehind.jpg");
    linkImageToMovie(movieId, "getoutdetailsbanner.jpg");
    linkImageToMovie(movieId, "getoutinterview.jpg");
    linkImageToMovie(movieId, "getoutposter.jpg");
    linkImageToMovie(movieId, "getouttrailer.jpg");

    releaseDate.set(1999, Calendar.MARCH, 31, 0, 0, 0);
    mDuration.set(0, 0, 0, 2, 16, 0);
    movieId = insertNewMovie("The Matrix",
                             "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.",
                             "R", "Wachowski Brothers",
                             "Keanu Reeves\nLaurence Fishburne\nCarrie-Anne Moss",
                             releaseDate,
                             "https://www.youtube.com/watch?v=vKQi3bBA1y8",
                             mDuration, 8.7, false, false, false, false, false);
    linkImageToMovie(movieId, "matrixbehind.jpg");
    linkImageToMovie(movieId, "matrixdetailsbanner.jpg");
    linkImageToMovie(movieId, "matrixinterview.jpg");
    linkImageToMovie(movieId, "matrixposter.jpg");
    linkImageToMovie(movieId, "matrixtrailer.jpg");

    releaseDate.set(1994, Calendar.JUNE, 24, 0, 0, 0);
    mDuration.set(0, 0, 0, 1, 28, 0);
    movieId = insertNewMovie("The Lion King",
                             "A Lion cub crown prince is tricked by a treacherous uncle into thinking he caused his father's death and flees into exile in despair, only to learn in adulthood his identity and his responsibilities.",
                             "G", "Roger Allers, Rob Minkoff",
                             "Matthew Broderick\nJeremy Irons\nJames Earl Jones",
                             releaseDate,
                             "https://www.youtube.com/watch?v=ie72bX3gas0",
                             mDuration, 8.5, false, false, true, false, false);
    linkImageToMovie(movieId, "thelionkingbehind.jpg");
    linkImageToMovie(movieId, "thelionkingdetailsbanner.jpg");
    linkImageToMovie(movieId, "thelionkinginterview.jpg");
    linkImageToMovie(movieId, "thelionkingposter.jpg");
    linkImageToMovie(movieId, "thelionkingtrailer.jpg");

    System.out.println("Finished adding movies to the database.");

    System.out.println("Adding time slots for each movie for the next week");

    createTimeSlots(7);

    System.out.println("Finished creating time slots");
  }

  /**
   * This adds a new image
   * @param image
   * @param name
   */
  public void addImage(InputStream image, String name) {
    insertNewImage(image, name.toLowerCase());
  }

  /**
   * Add seats for each timeSlot using its id
   *
   * @param id each timeSlot has an id
   */
  private void addSeats(int id, boolean vipSeatFlag) {
    String firstRecord = "INSERT INTO Seats (seatTakenFlag, vipSeatFlag, "
                         + "timeslot_seat) VALUES (:seatTakenFlag, "
                         + ":vipSeatFlag, :timeslot_seat)";
    try (Connection connection = db.beginTransaction()) {
      connection.createQuery(firstRecord).addParameter("seatTakenFlag", false)
                .addParameter("vipSeatFlag", vipSeatFlag)
                .addParameter("timeslot_seat", id).executeUpdate().getKey();
      connection.commit();
    }
  }

  /**
   * This adds timeSlots to each movie
   *
   * @param start
   * @param screen
   * @param movieId
   */
  public void addTimeSlots(Calendar start, int screen, int movieId) {
    Timestamp startTime = new Timestamp(start.getTimeInMillis());

    String firstRecord = "INSERT INTO TimeSlots (startTime, screen, "
                         + "timeslot_movieId) VALUES (:startTime, :screen, "
                         + ":timeslot_movieId)";
    try (Connection connection = db.beginTransaction()) {
      int createId = connection.createQuery(firstRecord)
                               .addParameter("startTime", startTime)
                               .addParameter("screen", screen)
                               .addParameter("timeslot_movieId", movieId)
                               .executeUpdate().getKey(Integer.class);
      connection.commit();
      // each timeSlot will have 80 seats standard seats
      for (int i = 1; i <= 80; i++) {
        addSeats(createId, false);
      }
      // each timeslot will have 20 vip seats
      for (int i = 1; i <= 20; i++) {
        addSeats(createId, true);
      }
    }
  }

  /**
   * This inserts a movie into the table using a transaction and then this is
   * used to insert an image into the Images Table
   *
   * @param name
   * @param blurb
   * @param certificate
   * @param director
   * @param lead_actors
   * @param release
   * @param youtubeLink
   * @param duration
   * @param rating
   * @param comingSoon
   * @param offer
   * @param family
   * @param student
   * @param classic
   * @return
   */
  public int insertNewMovie(String name, String blurb, String certificate,
                            String director, String lead_actors,
                            Calendar release, String youtubeLink,
                            Calendar duration, double rating,
                            boolean comingSoon, boolean offer, boolean family,
                            boolean student, boolean classic) {
    Timestamp releaseDate = new Timestamp(release.getTimeInMillis());
    Timestamp mDuration = new Timestamp(duration.getTimeInMillis());

    String firstRecord = "INSERT INTO MOVIES (name, blurb, certificate, "
                         + "director, lead_actors, releaseDate, youtubeLink, "
                         + "duration, rating, comingSoon, offer, family, "
                         + "student, classic) VALUES (:name, :blurb, "
                         + ":certificate, :director, :lead_actors, "
                         + ":releaseDate, :youtubeLink, :duration, :rating, "
                         + ":comingSoon, :offer, :family, :student, :classic)";
    try (Connection connection = db.beginTransaction()) {
      int createId = connection.createQuery(firstRecord)
                               .addParameter("name", name)
                               .addParameter("blurb", blurb)
                               .addParameter("certificate", certificate)
                               .addParameter("director", director)
                               .addParameter("lead_actors", lead_actors)
                               .addParameter("releaseDate", releaseDate)
                               .addParameter("youtubeLink", youtubeLink)
                               .addParameter("duration", mDuration)
                               .addParameter("rating", rating)
                               .addParameter("comingSoon", comingSoon)
                               .addParameter("offer", offer)
                               .addParameter("family", family)
                               .addParameter("student", student)
                               .addParameter("classic", classic).executeUpdate()
                               .getKey(Integer.class);
      connection.commit();
      return createId;
    }

  }

  /**
   * Links an image to a movie
   *
   * @param movieId The ID for the movie that needs to be linked
   * @param name The name of the image for this movie
   */
  private void linkImageToMovie(int movieId, String name) {
    String query = "UPDATE Images SET image_movie=:movieId WHERE "
                   + "imageId=:imageId";
    try (Connection connection = db.beginTransaction()) {
      connection.createQuery(query).addParameter("movieId", movieId)
                .addParameter("imageId", name).executeUpdate();
      connection.commit();
    }
  }

  /**
   * This function gives the movies in the database a slot to play in the cinema
   *
   * @param days Number of days from today to put movies in slots for
   */
  public void createTimeSlots(int days) {
    Calendar times = Calendar.getInstance();
    times.set(Calendar.HOUR_OF_DAY, 12);
    times.set(Calendar.MINUTE, 0);
    times.set(Calendar.SECOND, 0);
    times.set(Calendar.MILLISECOND, 0);

    List<Movies> movies = new ArrayList<Movies>();
    int totalRatings = 0;

    for (int slot = 1; slot < 12 * days + 1; slot++) {
      int screen = (slot % 3 == 0) ? 3 : slot % 3;
      if (!Main.database.timeSlotExists(times, screen)) {
        if (movies.isEmpty()) {
          totalRatings = 0;
          movies = Main.database.queryMovies();
          for (int i = movies.size() - 1; i >= 0; i--) {
            if (movies.get(i).getComingSoon()) {
              movies.remove(i);
            } else {
              totalRatings += movies.get(i).getRating() * 10;
            }
          }
        }

        int random = new Random().nextInt(totalRatings + 1);
        int currentRating = 0;
        for (int i = 0; i < movies.size(); i++) {
          currentRating += movies.get(i).getRating() * 10;
          if (currentRating >= random) {
            addTimeSlots(times, screen, movies.get(i).getMovieId());
            currentRating = 0;
            if (movies.size() + ((slot % 12 == 0) ? 12 : slot % 12) >= 13) {
              totalRatings -= movies.get(i).getRating() * 10;
              movies.remove(i);
            }
            break;
          }
        }
      }

      if (slot % 3 == 0) times.add(Calendar.HOUR_OF_DAY, 3);

      if (slot % 12 == 0) times.add(Calendar.HOUR_OF_DAY, 12);
    }
  }

  /**
   * This function creates slots for movies in a thread to prevent server from
   * stopping processing
   *
   * @param days Number of days from today to put movies in slots for
   */
  public void backgroundTimeSlots(int days) {
    if (!checkingTimeSlots) {
      checkingTimeSlots = true;
      new Thread(new Runnable() {
        @Override
        public void run() {
          createTimeSlots(days);
          checkingTimeSlots = false;
        }
      }).start();
    }
  }

  /**
   * inserts an image into database
   *
   * @param image this can be saved as a blob in the database
   */
  private void insertNewImage(InputStream image, String name) {
    String firstRecord = "INSERT INTO Images (imageId, image) VALUES "
                         + "(:imageId, :image)";
    try (Connection connection = db.beginTransaction()) {
      connection.createQuery(firstRecord).addParameter("imageId", name)
                .addParameter("image", image).executeUpdate();
      connection.commit();
    }
  }

  /**
   * Inserts a new staff member login , username and password but the password
   * is hashed to ensure it is safer
   *
   * @param userName
   * @param password
   * @param salt
   */
  public void insertNewStaff(String userName, byte[] password, byte[] salt) {
    String firstRecord = "INSERT INTO Staff (username, password, salt) VALUES "
                         + "(:username, :password, :salt)";
    try (Connection connection = db.beginTransaction()) {
      connection.createQuery(firstRecord).addParameter("username", userName)
                .addParameter("password", password).addParameter("salt", salt)
                .executeUpdate().getKey(Integer.class);
      connection.commit();
    }
  }

  /**
   * This creates the staff Table
   */
  public void createStaffTable() {
    Connection connection = db.beginTransaction();
    connection.createQuery("CREATE TABLE Staff (\n"
                           + "  staffId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,\n"
                           + "  username VARCHAR(100) NOT NULL,\n"
                           + "  password BLOB NOT NULL,\n"
                           + "  salt BLOB NOT NULL,\n"
                           + "  PRIMARY KEY (staffId)\n" + ")")
              .executeUpdate();
    connection.commit();
  }

  /**
   * This creates the Customers Table
   */
  public void createCustomerTable() {
    Connection connection = db.beginTransaction();
    connection.createQuery("CREATE TABLE Customers (\n"
                           + "  customersId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,\n"
                           + "  name VARCHAR(100) NOT NULL,\n"
                           + "  surname VARCHAR(100) NOT NULL,\n"
                           + "  cardNumber VARCHAR(100) DEFAULT NULL,\n"
                           + "  guestFlag BOOLEAN,\n"
                           + "  email VARCHAR(100),\n"
                           + "  username VARCHAR(100) DEFAULT NULL,\n"
                           + "  password BLOB DEFAULT NULL,\n"
                           + "  salt BLOB DEFAULT NULL,\n"
                           + "  PRIMARY KEY (customersId)\n" + ")")
              .executeUpdate();
    connection.commit();
  }

  /**
   * This creates the Movie Table
   */
  public void createMoviesTable() {
    Connection connection = db.beginTransaction();
    connection.createQuery("CREATE TABLE Movies (\n"
                           + "  movieId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,\n"
                           + "  name VARCHAR(100) NOT NULL,\n"
                           + "  blurb VARCHAR(1000) NOT NULL,\n"
                           + "  certificate VARCHAR(100) NOT NULL,\n"
                           + "  director VARCHAR(100) NOT NULL,\n"
                           + "  lead_actors VARCHAR (100) NOT NULL,\n"
                           + "  releaseDate TIMESTAMP,\n "
                           + "  youtubeLink VARCHAR (100) NOT NULL,\n"
                           + "  duration TIMESTAMP,\n"
                           + "  rating DOUBLE NOT NULL,\n"
                           + "  comingSoon BOOLEAN DEFAULT TRUE,\n"
                           + "  offer BOOLEAN DEFAULT FALSE,\n"
                           + "  family BOOLEAN DEFAULT FALSE,\n"
                           + "  student BOOLEAN DEFAULT FALSE,\n"
                           + "  classic BOOLEAN DEFAULT FALSE,\n"
                           + "  PRIMARY KEY (movieId)\n" + ")")
              .executeUpdate();
    connection.commit();
  }

  /**
   * This creates the images Table
   */
  public void createImagesTable() {
    Connection connection = db.beginTransaction();
    connection.createQuery("CREATE TABLE Images (\n"
                           + "  imageId VARCHAR(100) NOT NULL,\n"
                           + "  image BLOB,\n"
                           + "  image_movie INTEGER DEFAULT NULL,\n"
                           + "  PRIMARY KEY (imageId),\n"
                           + "  FOREIGN KEY (image_movie) REFERENCES Movies(movieId)\n"
                           + ")")
              .executeUpdate();
    connection.commit();
  }

  /**
   * This creates the TimeSlots table
   */
  public void createTimeSlotsTable() {
    Connection connection = db.beginTransaction();
    connection.createQuery("CREATE TABLE TimeSlots (\n"
                           + "  timeId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,\n"
                           + "  startTime TIMESTAMP,\n" + "  screen INTEGER,\n"
                           + "  timeSlot_movieId INTEGER,\n"
                           + "  PRIMARY KEY (timeId),\n"
                           + "  FOREIGN KEY (timeSlot_movieId) REFERENCES Movies(movieId)\n"
                           + ")")
              .executeUpdate();
    connection.commit();
  }

  /**
   * This creates the Seats Table
   */
  public void createSeatsTable() {
    Connection connection = db.beginTransaction();
    connection.createQuery("CREATE TABLE Seats (\n"
                           + "  seatsId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,\n"
                           + "  seatTakenFlag BOOLEAN DEFAULT FALSE,\n"
                           + "  vipSeatFlag BOOLEAN DEFAULT FALSE,\n"
                           + "  timeslot_seat INTEGER,\n"
                           + "  PRIMARY KEY (SeatsId),\n"
                           + "  FOREIGN KEY (timeSlot_seat) REFERENCES TimeSlots(timeId)\n"
                           + ")")
              .executeUpdate();
    connection.commit();
  }

  /**
   * This creates the Tickets Table
   */
  public void createTicketsTable() {
    Connection connection = db.beginTransaction();
    connection.createQuery("CREATE TABLE Tickets (\n"
                           + "  ticketId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,\n"
                           + "  timestamp TIMESTAMP,\n"
                           + "  ticket_seatId INTEGER,\n"
                           + "  customerType VARCHAR(100) NOT NULL,\n"
                           + "  paymentType VARCHAR(100) NOT NULL,\n"
                           + "  PRIMARY KEY (ticketId),\n"
                           + "  FOREIGN KEY (ticket_seatId) REFERENCES Seats(SeatsId)\n"
                           + ")")
              .executeUpdate();
    connection.commit();
  }

  /**
   * This creates the customer_movies Table
   */
  public void createCustomer_MoviesTable() {
    Connection connection = db.beginTransaction();
    connection.createQuery("CREATE TABLE Customer_Movies (\n"
                           + "  customerMoviesId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,\n"
                           + "  customerMovies_movieId INTEGER,\n"
                           + "  customerMovies_customerId INTEGER,\n"
                           + "  customerMovies_ticketId INTEGER,\n"
                           + "  customerMovies_staffId VARCHAR(20) DEFAULT NULL,\n"
                           + "  PRIMARY KEY (customerMoviesId),\n"
                           + "  FOREIGN KEY (customerMovies_movieId) REFERENCES Movies(movieId),\n"
                           + "  FOREIGN KEY (customerMovies_customerId) REFERENCES Customers(customersId),\n"
                           + "  FOREIGN KEY (customerMovies_ticketId) REFERENCES Tickets (ticketId)\n"
                           + ")")
              .executeUpdate();
    connection.commit();
  }
}
