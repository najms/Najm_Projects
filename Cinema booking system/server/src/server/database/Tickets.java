package server.database;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * This holds the ticket information and an id for each customer and seat number
 * given/selected
 */
public class Tickets {
  private int ticketId;
  private Timestamp timestamp;
  private String customerType;
  private String paymentType;
  private int ticket_seatId;

  /**
   * getter for the ticketId
   * @return ticketId
   */
  public int getTicketId() {

    return ticketId;
  }

  /**
   * getter for the Timestamp
   * @return time
   */
  public Calendar getTimestamp() {
    Calendar time = Calendar.getInstance();
    time.setTimeInMillis(timestamp.getTime());
    return time;
  }

  /**
   * getter for the ticket_seatId links tickets and seats
   * @return ticket_seatId
   */
  public int getTicket_seatId() {

    return ticket_seatId;
  }

  /**
   * getter for the customerType
   * @return customerType
   */
  public String getCustomerType() {

    return customerType;
  }

  /**
   * getter for the paymentType
   * @return paymentType
   */
  public String getPaymentType() {

    return paymentType;
  }

}
