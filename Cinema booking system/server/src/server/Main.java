package server;

import server.authentication.UserLogin;
import server.connection.ServerConnection;
import server.database.DatabaseHandler;

/**
 *
 */
public class Main {

  /**
   *
   */
  public static ServerConnection connection;
  /**
   *
   */
  public static DatabaseHandler database;
  /**
   *
   */
  public static UserLogin login;

  /**
   * @param args
   */
  public static void main(String[] args) {
    login = new UserLogin();
    database = new DatabaseHandler();
    database.startDatabase();

    if (args.length > 0)
      if ("demo".equalsIgnoreCase(args[0]))
        Demo.setUp();

    server.gui.Gui.start();
    connection = new ServerConnection();
    try {
      connection.run();
    } catch (Exception e) {
      System.err.println(e);
    }
  }

}
