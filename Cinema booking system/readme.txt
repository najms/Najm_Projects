Merlin Group Project
Members: Najmuddin, Asim, Daniel, Sulaiman & Ali

How To Run
Server and Database:
first, cd into server folder, run ant superclean followed by ant rundemo.

Javafx:
after running the server, cd into client folder and run ant clean followed by ant run.
try username:najm password:najm or username:ali password:ali to get pass the login.
if this doesnt work, wait a moment for the server to fully load then try again.

Website:
Type Https://localhost:8080 on a browser of your choice after running server

Note:
Found small typo after demo and added the GUI on 4 May 2018.
