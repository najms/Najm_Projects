#include "Snow.h"

/*-----------------------------------------------------------------------------
            Class constructor, intialises the x,y,z of the snow particle.
-----------------------------------------------------------------------------*/
Snow::Snow(float x1, float y1, float z1){
    this->x=x1;
    this->y=y1;
    this->z=z1;
}

/*-----------------------------------------------------------------------------
    Decrements the y value to show a falling effect but only if falling is true.
    falling is set to false in the case where the snow particle lands on the floor
    so that the snow particle doesnt move anymore and this gives the effect of snow
    accumalating on the floor.
-----------------------------------------------------------------------------*/
void Snow::move() {
    if(falling==true){
        y -= 0.1;
    }
}

/*-----------------------------------------------------------------------------
    Renders a small white sphere on the screen which required temporarily
    disabling GL_TEXTURE_2D.
-----------------------------------------------------------------------------*/
void Snow::draw(){
    glDisable(GL_TEXTURE_2D);

   glPushMatrix();
       glTranslatef( x, y, z );
       GLfloat qaLightPosition[] = {1, 1, 1, 1};
       glLightfv( GL_LIGHT0, GL_POSITION, qaLightPosition );
       GLfloat emitLight[] = {0.9, 0.9, 0.9, 0.9};
       glMaterialfv( GL_FRONT_AND_BACK, GL_EMISSION, emitLight );
       glutSolidSphere( 0.1, 20, 20 );
       GLfloat Noemit[] = {0.0, 0.0, 0.0, 1.0};
       glMaterialfv( GL_FRONT_AND_BACK, GL_EMISSION, Noemit );
   glPopMatrix();

   glEnable(GL_TEXTURE_2D);
}
