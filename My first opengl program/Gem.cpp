#include "Gem.h"

/*-----------------------------------------------------------------------------
            Class constructor, intialises the x,y,z of the gem.
-----------------------------------------------------------------------------*/
Gem::Gem(float x1, float y1, float z1){
    this->x=x1;
    this->y=y1;
    this->z=z1;
}

/*-----------------------------------------------------------------------------
    Decrements the y value of the gem and rotates it slightly to display an
    animation of falling gems from the sky.
-----------------------------------------------------------------------------*/
void Gem::move() {
    if(falling==true){
        y -= 0.1;
        this->angle=(this->angle+4)%360;
    }
}

/*-----------------------------------------------------------------------------
    Calls the relevant opengl functions to position the gem in its x,y,z
    coordinates and render it on the screen. While doing so, also rotates the
    shape and scales it down so that its smaller.
-----------------------------------------------------------------------------*/
void Gem::draw(){
   glPushMatrix();
       glTranslatef( x, y, z );
       glScalef(0.2, 0.2, 0.2);
       glRotatef(angle, 0., 1., 0.);
       this->displayConvex();
   glPopMatrix();
}

/*-----------------------------------------------------------------------------
    Creates a octahedron by first calling the helper function drawPyramid to
    create the top part of the shape and then translate and rotates the modelview
    matrix by 180 degrees so that the second time drawPyramid is called, it will
    complete the bottom half of the octahedron.
-----------------------------------------------------------------------------*/
void Gem::displayConvex(){
    //render first half of octahedron
    glPushMatrix();
    glTranslatef(0., 1.0, 0.);
    this->drawPyramid();

    //render second half to complete the shape
    glTranslatef(0., -2.0, 0.);
    glRotatef(180,1.0, 0.,0.);
    this->drawPyramid();
    glPopMatrix();
}

/*-----------------------------------------------------------------------------
    Creates a 4 sided pyramid from GL Triangles and assigns a color to each
    of the vertices, for this to work, GL Lighting is temporarily disabled.
-----------------------------------------------------------------------------*/
void Gem::drawPyramid(){
    glDisable(GL_LIGHTING);
    glBegin(GL_TRIANGLES);

    //side 1
    glColor3f((1.0/255.)*31,   (1.0/255.)*171,    (1.0/255.)*137);
    glVertex3f( 0.0f, 1.0f, 0.0f);
    glColor3f((1.0/255.)*85,   (1.0/255.)*233,    (1.0/255.)*188);
    glVertex3f(-1.0f,-1.0f, 1.0f);
    glColor3f((1.0/255.)*46,   (1.0/255.)*184,    (1.0/255.)*114);
    glVertex3f( 1.0f,-1.0f, 1.0f);

    //side 2
    glColor3f((1.0/255.)*31,   (1.0/255.)*171,    (1.0/255.)*137);
    glVertex3f( 0.0f, 1.0f, 0.0f);
    glColor3f((1.0/255.)*46,   (1.0/255.)*184,    (1.0/255.)*114);
    glVertex3f( 1.0f,-1.0f, 1.0f);
    glColor3f((1.0/255.)*85,   (1.0/255.)*233,    (1.0/255.)*188);
    glVertex3f( 1.0f,-1.0f,-1.0f);

    //side 3
    glColor3f((1.0/255.)*31,   (1.0/255.)*171,    (1.0/255.)*137);
    glVertex3f( 0.0f, 1.0f, 0.0f);
    glColor3f((1.0/255.)*85,   (1.0/255.)*233,    (1.0/255.)*188);
    glVertex3f( 1.0f,-1.0f,-1.0f);
    glColor3f((1.0/255.)*46,   (1.0/255.)*184,    (1.0/255.)*114);
    glVertex3f(-1.0f,-1.0f,-1.0f);

    //side 4
    glColor3f((1.0/255.)*31,   (1.0/255.)*171,    (1.0/255.)*137);
    glVertex3f( 0.0f, 1.0f, 0.0f);
    glColor3f((1.0/255.)*46,   (1.0/255.)*184,    (1.0/255.)*114);
    glVertex3f(-1.0f,-1.0f,-1.0f);
    glColor3f((1.0/255.)*85,   (1.0/255.)*233,    (1.0/255.)*188);
    glVertex3f(-1.0f,-1.0f, 1.0f);
    
    glEnd();
    glEnable(GL_LIGHTING);
}
