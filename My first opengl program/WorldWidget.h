#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1
#include <QGLWidget>
#include <GL/glut.h>
#include <math.h>
#include <QtGui>
#include "Image.h"
#include "Snow.h"
#include "Gem.h"
#include <vector>

/*-----------------------------------------------------------------------------
    Header file and class declaration for WorldWidget.cpp. This class extends
    a QT widget which enables it to run opengl code alongside QT. This class
    includes all the core elements and logic of the program mainly the OpenGL
    graphics code. The class creates a floor in which a person/ghost moves around
    it, renders a rotating earth at the top and also has falling snow and gems effect.
-----------------------------------------------------------------------------*/
class WorldWidget: public QGLWidget{
    Q_OBJECT
	public:
    	WorldWidget(QWidget *parent);
        ~WorldWidget();

    protected:
    	void initializeGL();
    	void resizeGL(int w, int h);
    	void paintGL();
        void createCrate();
        void mouseMoveEvent(QMouseEvent *event);
        void mousePressEvent(QMouseEvent *eventPress);
        void displayMiddleCube();
        void displayGemsAndSnow();
        void movingPerson();
        void setMaterial(int style);   //brass=0   white=1     red=2
        void displayFloor();

    public slots:
        void sliderUpdate(int value);
        void changePersonSpeed(int value);
        void changeMouseSensitivty(int value);
        void changeSnowAmount(int value);
        void personUpdate();
        void snowUpdate();
        void gemsUpdate();

	private:
        Image   floorsides; //Image textures
        Image   floortop;
        Image   earth;
        Image   marc;
        Image   crate;
        GLuint tex_obj[5];

        QTimer *timer_person;   //timers for animation
        QTimer *timer_snow;
        QTimer *timer_gems;
        float mouse_sensitivity=0.0002;
        GLUquadricObj *sphere=NULL;

        std::vector<Snow> particles;    //vectors to store snow/gem particles
        std::vector<Gem> gems;

        int angle;          //variables for the moving ghost animation
        int slidervalue=0;
        int side=1;
        float base[3]={0.0, 0.0,    0.0};
        float rotatebody=0;
        int armangle_right=45;
        int armangle_left=45;
        bool arm_forward[2]={true, true};
        bool floating_ghost=true;

        float camera[3];    //variables for moving the camera with the mouse
        int oldx=0, oldy=0;
        float horiz_angle=0.0, vert_angle=0.0;

    	GLUquadricObj* phead=NULL;     //GLUquadricObj for creating the ghost object
    	GLUquadricObj* ptorso;
    	GLUquadricObj* pshoulders;
    	GLUquadricObj* pupperleft1;
    	GLUquadricObj* plowerleft1;
        GLUquadricObj* pupperleft2;
    	GLUquadricObj* plowerleft2;
};

/*-----------------------------------------------------------------------------
                        Setting up material properties.
-----------------------------------------------------------------------------*/
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;

static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.0},
  { 0.78, 0.57, 0.11, 1.0},
  { 0.99, 0.91, 0.81, 1.0},
  27.8
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0
};

static materialStruct red_shiny_plastic {
    {0.3, 0.0, 0.0, 1.0}, // Ka
    {0.6, 0.0, 0.0, 1.0}, // Kd
    {0.8, 0.6, 0.6, 1.0}, // Ks
    100.0 // n
};
#endif
