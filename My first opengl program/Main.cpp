#include <QApplication>
#include <QVBoxLayout>
#include "WorldWindow.h"

/*-----------------------------------------------------------------------------
                    Main function of the program.
-----------------------------------------------------------------------------*/
int main(int argc, char *argv[]){
	// create the application
	QApplication app(argc, argv);
    glutInit(&argc, argv);

	// create a master widget
   	WorldWindow *window = new WorldWindow(NULL);
	window->resize(512, 612); 	// resize the window
	window->show(); // show the label
	app.exec(); // start it running

	// clean up
	delete window;
	return 0;
}
